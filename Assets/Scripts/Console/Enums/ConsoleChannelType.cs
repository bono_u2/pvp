﻿public enum ConsoleChannelType
{
    Default,
    Resources,
    Config,
    Network,
    Debug,
    Stopwatch,
    Benchmark,
    MemoryLeak,
    Advertising,
    UI,
    Integration,
    Purchase,
}