﻿using FreeTeam.Events;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Console : EventDispatcher
{
    #region Instance
    private static Console instance_;
    private static Console Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new Console ();

            return instance_;
        }
    }
    #endregion

    public Console () { }

    #region Get / Set
    public List<ConsoleLog> FullLog { get; } = new List<ConsoleLog> ();

    public int MaxLogsCount { get; set; } = 256;
    #endregion

    #region Private methods 
    private void Add (ConsoleLog _consoleLog)
    {
        if (FullLog.Count >= MaxLogsCount)
            FullLog.RemoveAt (0);

        FullLog.Add (_consoleLog);

        Dispatch (new ConsoleLogEvent (_consoleLog));
    }

    private void Clear ()
    {
        FullLog.Clear ();
    }
    #endregion

    #region Public static methods
    public static void Log (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log ($"<color=#eeeeee><b>[Log]:</b></color> {text}");
#endif

        Instance.Add (new ConsoleLog (ConsoleLogType.Log, text, _channel));
    }

    public static void Message (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log ($"<color=#99BB33><b>[Message]:</b></color> {text}");
#endif

        Instance.Add (new ConsoleLog (ConsoleLogType.Message, text, _channel));
    }

    public static void Info (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log ($"<color=#5998ff><b>[Info]:</b></color> {text}");
#endif

        Instance.Add (new ConsoleLog (ConsoleLogType.Info, text, _channel));
    }

    public static void Warning (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log ($"<color=#ffcb49><b>[Warning]:</b></color> {text}");
#endif

        Instance.Add (new ConsoleLog (ConsoleLogType.Warning, text, _channel));
    }

    public static void Error (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log ($"<color=#b50707><b>[Error]:</b></color> {text}");
#endif

        Instance.Add (new ConsoleLog (ConsoleLogType.Error, text, _channel));
    }

    public static List<ConsoleLog> GetFullConsoleLog ()
    {
        return Instance.FullLog;
    }

    public static void ClearLog ()
    {
        Instance.Clear ();
    }
    #endregion
}