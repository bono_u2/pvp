﻿using System;

public class ConsoleLog
{
    public ConsoleLog (ConsoleLogType _logType, string _text = "", ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        Date = DateTime.Now;
        LogType = _logType;
        Text = _text;
        Channel = _channel;
    }

    #region Get / Set
    public DateTime Date { get; private set; }
    public ConsoleLogType LogType { get; private set; }
    public string Text { get; private set; }
    public ConsoleChannelType Channel { get; private set; }
    #endregion
}