﻿using Configurations;

public interface IAmmo
{
    AmmoConfig Config { get; }
}