﻿using Configurations;
using FreeTeam.Events;

public interface ICharacter : IEventDispatcher
{
    CharacterConfig Config { get; }

    IWeapon Weapon { get; }

    float Health { get; }

    void Damage (IAmmo _ammo);
}