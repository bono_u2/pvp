﻿using Configurations;
using FreeTeam.Events;
using UnityEngine;

public interface IWeapon : IEventDispatcher
{
    WeaponConfig Config { get; }

    IAmmo Ammo { get; }

    bool IsReady { get; }
    bool IsReloaded { get; }

    int AmmoAmount { get; }

    void Fire (Vector3 _position, Vector3 _direction);
    void FixedUpdate ();
}