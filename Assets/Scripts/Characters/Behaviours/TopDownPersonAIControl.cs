﻿using UnityEngine;

[RequireComponent (typeof (TopDownPersonCharacter))]
public class TopDownPersonAIControl : MonoBehaviour
{
    #region Vars
    private TopDownPersonCharacter topDownPersonCharacter;
    private TopDownPersonControl player;

    private float attackDistance;
    private float rndAngle;
    private Vector3 offset;

    private Vector3 moveVector = Utils.VectorUtils.ZERO;
    private Vector3 rotationVector = Utils.VectorUtils.FORWARD;
    #endregion

    #region Unity methods
    void Start ()
    {
        player = FindObjectOfType<TopDownPersonControl> ();
    }

    void FixedUpdate ()
    {
        if (topDownPersonCharacter)
        {
            topDownPersonCharacter.Move (moveVector);
            topDownPersonCharacter.Rotate (rotationVector);
        }
    }

    void Update ()
    {
        topDownPersonCharacter.Attack = CanAttack;

        UpdateMove ();
        UpdateRotation ();
    }
    #endregion

    #region Get / Set
    private float DistanceToPlayer { get { return (player.transform.position - transform.position).magnitude; } }
    private bool CanAttack { get { return DistanceToPlayer <= attackDistance && player.IsAlive; } }
    #endregion

    #region Public methods
    public void Init ()
    {
        topDownPersonCharacter = GetComponent<TopDownPersonCharacter> ();

        var dist = topDownPersonCharacter.Weapon.Config.Distance;

        attackDistance = dist * 0.5f + Utils.MathUtils.RandRange (0f, dist * 0.5f);
        rndAngle = Utils.MathUtils.RandRange (0, 360);
        offset = Quaternion.AngleAxis (rndAngle, Utils.VectorUtils.UP) * Utils.VectorUtils.FORWARD * attackDistance;
    }
    #endregion

    #region Private methods
    private void UpdateMove ()
    {
        if (player && !CanAttack)
        {
            var direction = (player.transform.position + offset) - transform.position;
            moveVector = (direction.magnitude > attackDistance) ? direction.normalized : direction / attackDistance;
        }
    }

    private void UpdateRotation ()
    {
        if (player)
            rotationVector = (player.transform.position - transform.position).normalized;
    }
    #endregion
}