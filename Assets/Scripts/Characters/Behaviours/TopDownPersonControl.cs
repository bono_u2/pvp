﻿using UnityEngine;

[RequireComponent (typeof (TopDownPersonCharacter))]
public class TopDownPersonControl : MonoBehaviour
{
    #region Vars
    private TopDownPersonCharacter topDownPersonCharacter;
    private BattleInputScreenController battleInputScreenController;

    private Vector3 moveVector = Utils.VectorUtils.ZERO;
    private Vector3 rotationVector = Utils.VectorUtils.FORWARD;
    #endregion

    #region Unity methods
    void Start ()
    {
        topDownPersonCharacter = GetComponent<TopDownPersonCharacter> ();
        battleInputScreenController = FindObjectOfType<BattleInputScreenController> ();

        battleInputScreenController.RightStick.AddListener<StickStartDragEvent> (OnShotStickStartDragHandler);
        battleInputScreenController.RightStick.AddListener<StickEndDragEvent> (OnShotStickEndDragHandler);
    }

    void FixedUpdate ()
    {
        if (topDownPersonCharacter)
        {
            topDownPersonCharacter.Move (moveVector);
            topDownPersonCharacter.Rotate (rotationVector);
        }
    }

    void Update ()
    {
        if (IsAlive)
        {
            UpdateMove ();
            UpdateRotation ();
        }
        else
        {
            moveVector = Utils.VectorUtils.ZERO;
            rotationVector = Utils.VectorUtils.FORWARD;
        }
    }
    #endregion

    #region Get / Set
    public bool IsAlive { get { return topDownPersonCharacter && topDownPersonCharacter.IsAlive; } }
    #endregion

    #region Private methods
    private void UpdateMove ()
    {
        if (battleInputScreenController)
            moveVector = Utils.VectorUtils.RIGHT * battleInputScreenController.LeftStickDirection.x + Utils.VectorUtils.FORWARD * battleInputScreenController.LeftStickDirection.y;
        else
            moveVector = Utils.VectorUtils.ZERO;
    }

    private void UpdateRotation ()
    {
        if (battleInputScreenController)
        {
            var rotation = Utils.VectorUtils.RIGHT * battleInputScreenController.RightStickDirection.x + Utils.VectorUtils.FORWARD * battleInputScreenController.RightStickDirection.y;
            if (rotation != Utils.VectorUtils.ZERO)
                rotationVector = rotation;
        }
        else
            rotationVector = Utils.VectorUtils.FORWARD;
    }

    private void OnShotStickStartDragHandler (FreeTeam.Events.Event _event)
    {
        topDownPersonCharacter.Attack = true;
    }

    private void OnShotStickEndDragHandler (FreeTeam.Events.Event _event)
    {
        topDownPersonCharacter.Attack = false;
    }
    #endregion
}