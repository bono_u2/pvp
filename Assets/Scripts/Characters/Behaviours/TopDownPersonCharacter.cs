﻿using UnityEngine;

public class TopDownPersonCharacter : MonoBehaviour
{
    #region Vars
    private Rigidbody rb;
    #endregion

    #region Unity methods
    void Start ()
    {
        rb = GetComponent<Rigidbody> ();
    }

    void FixedUpdate ()
    {
        if (Weapon != null)
            Weapon.FixedUpdate ();
    }

    void Update ()
    {
        if (Attack && Weapon.IsReady)
            Weapon.Fire (transform.position + transform.forward * 0.8f + Vector3.up * 0.5f, transform.forward);
    }

    void OnTriggerEnter (Collider _other)
    {
        var bullet = _other.GetComponent<Bullet> ();
        if (bullet)
            Character.Damage (bullet.Ammo);
    }
    #endregion

    #region Get / Set
    public ICharacter Character { get; private set; }
    public IWeapon Weapon { get; private set; }

    public ISpawner Spawner { get; private set; }

    public bool Attack { get; set; }
    public bool IsAlive { get { return Character.Health > 0; } }
    #endregion

    #region Public methods
    public void Init (ICharacter _character, ISpawner _spawner)
    {
        Character = _character;
        Weapon = Character.Weapon;
        Spawner = _spawner;

        Character.AddListener<CharacterDieEvent> (OnCharacterDieHandler);
    }

    public void Move (Vector3 _moveVector)
    {
        rb.velocity = _moveVector * Character.Config.Speed;
        rb.angularVelocity = Utils.VectorUtils.ZERO;
    }

    public void Rotate (Vector3 _rotationVector)
    {
        if (_rotationVector != Utils.VectorUtils.ZERO)
            rb.rotation = Quaternion.LookRotation (_rotationVector);
    }
    #endregion

    #region Private methods
    private void OnCharacterDieHandler (FreeTeam.Events.Event _event)
    {
        Character.RemoveListener<CharacterDieEvent> (OnCharacterDieHandler);

        Spawner.Remove (this);
    }
    #endregion
}