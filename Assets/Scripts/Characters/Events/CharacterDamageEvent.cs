﻿using FreeTeam.Events;

public class CharacterDamageEvent : Event
{
    #region Constants
    public const string ID = "CharacterDamage";
    #endregion

    public CharacterDamageEvent (object _sender, float _health) : base (_sender, ID)
    {
        Health = _health;
    }

    #region Get / Set
    public float Health { get; private set; }
    #endregion
}