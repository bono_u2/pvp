﻿using FreeTeam.Events;

public class CharacterDieEvent : Event
{
    #region Constants
    public const string ID = "EnemyDie";
    #endregion

    public CharacterDieEvent (object _sender) : base (_sender, ID) { }
}