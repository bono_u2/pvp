﻿using FreeTeam.Events;

public class WeaponAmmoAmountChangedEvent : Event
{
    #region Constants
    public const string ID = "WeapponAmmoAmountChanged";
    #endregion

    public WeaponAmmoAmountChangedEvent (object _sender, int _ammoAmont) : base (_sender, ID)
    {
        AmmoAmount = _ammoAmont;
    }

    #region Get / Set
    public int AmmoAmount { get; private set; }
    #endregion
}