﻿using Configurations;
using FreeTeam.Events;

public class Character : EventDispatcher, ICharacter
{
    public Character (CharacterConfig _config)
    {
        Config = _config;

        Init ();
    }

    #region Get / Set
    public CharacterConfig Config { get; private set; }

    public IWeapon Weapon { get; private set; }

    public float Health { get; private set; }
    #endregion

    #region Public methods
    public void Damage (IAmmo _ammo)
    {
        if (Health > 0)
        {
            Health -= _ammo.Config.Damage;
            Health = UnityEngine.Mathf.Max (Health, 0);

            Dispatch (new CharacterDamageEvent (this, Health));

            if (Health <= 0)
                Dispatch (new CharacterDieEvent (this));

            Console.Info ($"Health: {Health}, Damage: {_ammo.Config.Damage}");
        }
    }
    #endregion

    #region Private methods
    private void Init ()
    {
        Weapon = new Weapon (Config.WeaponConfig ());

        Health = Config.Health;
    }
    #endregion
}