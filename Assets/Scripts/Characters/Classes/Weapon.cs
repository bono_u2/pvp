﻿using Configurations;
using FreeTeam.Events;
using UnityEngine;

public class Weapon : EventDispatcher, IWeapon
{
    #region Vars
    private GameObject bulletPrefab;

    private float reloadTime = 0.0f;
    private float delayTime = 0.0f;

    private int ammoAmount = 0;
    #endregion

    public Weapon (WeaponConfig _config)
    {
        Config = _config;

        Init ();
    }

    #region Get / Set
    public WeaponConfig Config { get; private set; }

    public IAmmo Ammo { get; private set; }

    public bool IsReady => (delayTime <= 0.0f && !IsReloaded);
    public bool IsReloaded => (reloadTime > 0.0f);

    public int AmmoAmount
    {
        get { return ammoAmount; }
        private set
        {
            ammoAmount = value;

            Dispatch (new WeaponAmmoAmountChangedEvent (this, AmmoAmount));
        }
    }
    #endregion

    #region Public methods
    public void FixedUpdate ()
    {
        var dt = Time.fixedDeltaTime;

        if (reloadTime > 0.0f)
            reloadTime -= Time.fixedDeltaTime;
        else if (reloadTime <= 0.0f && AmmoAmount <= 0)
            AmmoAmount = Config.CartridgeSize;

        if (delayTime > 0.0f)
            delayTime -= dt;
    }

    public void Fire (Vector3 _position, Vector3 _direction)
    {
        if (!IsReady)
            return;

        var bullet = ObjectPoolManager.SpawnObject (bulletPrefab, _position, Quaternion.identity).GetComponent<Bullet> ();
        bullet.Init (Ammo, _direction);

        AmmoAmount -= Config.ExpensePerShot;

        if (AmmoAmount <= 0)
            reloadTime = Config.ReloadTime;

        delayTime = Config.RateOfFire;
    }
    #endregion

    #region Private methods
    private void Init ()
    {
        Ammo = new Ammo (Config.AmmoConfig ());

        AmmoAmount = Config.CartridgeSize;

        bulletPrefab = ResourcesManager.LoadCache<GameObject> (URIs.AMMO_PREFABS_URI, Ammo.Config.Prefab);
        ObjectPoolManager.WarmPool (bulletPrefab, Config.CartridgeSize);
    }
    #endregion
}