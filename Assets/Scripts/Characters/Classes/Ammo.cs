﻿using Configurations;

public class Ammo : IAmmo
{
    public Ammo (AmmoConfig _config)
    {
        Config = _config;
    }

    #region Get / Set
    public AmmoConfig Config { get; private set; }
    #endregion
}