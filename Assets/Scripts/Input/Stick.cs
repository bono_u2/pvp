﻿using UnityEngine.EventSystems;
using UnityEngine;

public class Stick : EventMonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    #region Vars
    public RectTransform background;
    public RectTransform handle;

    private float radius;
    private Vector2 joystickPosition;
    #endregion

    #region Unity methods
    void Start ()
    {
        radius = background.sizeDelta.x * 0.5f;
        joystickPosition = handle.position;
    }
    #endregion

    #region Get / Set
    public Vector2 Direction { get; private set; } = Utils.VectorUtils.ZERO;
    #endregion

    #region Public methods
    public void OnPointerDown (PointerEventData _eventData)
    {
        Dispatch (new StickStartDragEvent (this, _eventData));

        OnDrag (_eventData);
    }

    public void OnDrag (PointerEventData _eventData)
    {
        var direction = _eventData.position - joystickPosition;
        Direction = (direction.magnitude > radius) ? direction.normalized : direction / radius;
        handle.anchoredPosition = (Direction * radius);

        Dispatch (new StickDragedEvent (this, _eventData));
    }

    public void OnPointerUp (PointerEventData _eventData)
    {
        Dispatch (new StickEndDragEvent (this, _eventData));

        Direction = Utils.VectorUtils.ZERO;
        handle.anchoredPosition = Utils.VectorUtils.ZERO;
    }
    #endregion
}