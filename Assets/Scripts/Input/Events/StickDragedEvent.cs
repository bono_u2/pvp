﻿using UnityEngine.EventSystems;

public class StickDragedEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "StickDraged";
    #endregion

    public StickDragedEvent (object _sender, PointerEventData _pointerEventData) : base (_sender, ID)
    {
        PointerEventData = _pointerEventData;
    }

    #region Get / Set
    public PointerEventData PointerEventData { get; private set; }
    #endregion
}