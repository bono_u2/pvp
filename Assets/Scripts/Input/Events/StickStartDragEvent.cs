﻿using UnityEngine.EventSystems;

public class StickStartDragEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "StickStartDrag";
    #endregion

    public StickStartDragEvent (object _sender, PointerEventData _pointerEventData) : base (_sender, ID)
    {
        PointerEventData = _pointerEventData;
    }

    #region Get / Set
    public PointerEventData PointerEventData { get; private set; }
    #endregion
}