﻿using UnityEngine.EventSystems;

public class StickEndDragEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "StickEndDrag";
    #endregion

    public StickEndDragEvent (object _sender, PointerEventData _pointerEventData) : base (_sender, ID)
    {
        PointerEventData = _pointerEventData;
    }

    #region Get / Set
    public PointerEventData PointerEventData { get; private set; }
    #endregion
}