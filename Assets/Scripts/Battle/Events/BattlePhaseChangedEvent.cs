﻿using FreeTeam.Events;

public class BattlePhaseChangedEvent : Event
{
    #region Constants
    public const string ID = "BattlePhaseChanged";
    #endregion

    public BattlePhaseChangedEvent (object _sender, BattlePhase _newPhase, BattlePhase _prevPhase) : base (_sender, ID)
    {
        NewPhase = _newPhase;
        PrevPhase = _prevPhase;
    }

    #region Get / Set
    public BattlePhase NewPhase { get; private set; }
    public BattlePhase PrevPhase { get; private set; }
    #endregion
}