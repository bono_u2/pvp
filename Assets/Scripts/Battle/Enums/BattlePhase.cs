﻿public enum BattlePhase
{
    Undefined,
    Ready,
    During,
    Win,
    Lose,
    Pause,
    Abort,
}