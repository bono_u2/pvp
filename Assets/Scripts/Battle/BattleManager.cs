﻿using Configurations;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;

public class BattleManager : EventMonoBehaviour
{
    #region Singleton
    private static BattleManager instance_;
    public static BattleManager Instance
    {
        get
        {
            if (!instance_)
                instance_ = FindObjectOfType<BattleManager> ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private float saveTimeScale = 1.0f;
    private BattlePhase phase = BattlePhase.Undefined;

    private EnemySpawner[] enemySpawners;
    private float waveDelayTime;
    #endregion

    #region Unity methods
    void FixedUpdate ()
    {
        if (Phase == BattlePhase.During && CurrentWave < Config.WavesAmount)
        {
            if (waveDelayTime <= 0.0f)
            {
                GenerateWave ();

                waveDelayTime = Config.WaveDelay;
            }
            else
                waveDelayTime -= Time.fixedDeltaTime;
        }
    }

    void LateUpdate ()
    {
        BattleCheck ();
    }
    #endregion

    #region Get / Set
    public BattlePhase Phase
    {
        get { return phase; }
        set
        {
            Dispatch (new BattlePhaseChangedEvent (this, value, phase));

            Console.Message ($"<b>BattlePhaseChanged: <i>{phase.ToString ()}</i> -> <i>{value.ToString ()}</i></b>");

            phase = value;
        }
    }

    public bool IsPaused { get; private set; } = false;

    public MissionConfig Config { get; private set; }

    public int CurrentWave { get; private set; }
    #endregion

    #region Public methods
    public void PauseBattle ()
    {
        if (IsPaused)
            return;

        IsPaused = true;

        saveTimeScale = Time.timeScale;

        Time.timeScale = 0.0f;
    }

    public void ResumeBattle ()
    {
        if (!IsPaused)
            return;

        IsPaused = false;

        Time.timeScale = saveTimeScale;
    }
    #endregion

    #region Private methods
    private void GenerateWave ()
    {
        for (var i = 0; i < enemySpawners.Length; i++)
        {
            var enemyCount = Config.EnemyCount ();
            enemySpawners[i].AddToQueue (enemyCount);
        }

        CurrentWave++;
    }

    private void BattleCheck ()
    {
        //if () Phase = BattlePhase.Win;
    }
    #endregion

    #region Coroutines
    public IEnumerator Init (MissionConfig _config)
    {
        Config = _config;

        var watch = new System.Diagnostics.Stopwatch ();
        watch.Start ();


        // ищем спаунер для игрока и инитем его
        var playerSpawner = FindObjectOfType<PlayerSpawner> ();
        yield return playerSpawner.Init ();

        // помещаем префабы врагов в ObjectPool
        yield return PreloadEnemies ();

        // инициализируем точки появления врагов
        yield return InitSpawners ();

        // инициализируем камеру
        var gameCamera = FindObjectOfType<GameCamera> ();
        yield return gameCamera.Init ();

        // выставляем фазу
        Phase = BattlePhase.Ready;


        watch.Stop ();
        Console.Info ($"<b>({this.GetType ().ToString ()})</b> InitTime: <b><i>{watch.Elapsed.TotalSeconds:N6}s.</i></b>", ConsoleChannelType.Stopwatch);

        yield return null;
    }

    private IEnumerator PreloadEnemies ()
    {
        for (var i = 0; i < Config.Enemies.Length; i++)
        {
            var characterId = Config.Enemies[i];
            var characterConfig = GlobalData.Config.Characters.FirstOrDefault (x => x.Id == characterId);
            if (characterConfig == null)
                continue;

            var prefab = ResourcesManager.LoadCache<GameObject> (URIs.CHARACTER_PREFABS_URI, characterConfig.Prefab);
            if (prefab == null)
                throw new Exception ("Player Prefab not found!");

            ObjectPoolManager.WarmPool (prefab, 5);
        }

        yield return null;
    }

    private IEnumerator InitSpawners ()
    {
        enemySpawners = FindObjectsOfType<EnemySpawner> ();

        for (var i = 0; i < enemySpawners.Length; i++)
            yield return enemySpawners[i].Init (Config);

        yield return null;
    }
    #endregion
}