﻿namespace FreeTeam.Events
{
    public interface IEvent
    {
        object Sender { get; }
        string Type { get; }
    }
}