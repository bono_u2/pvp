﻿namespace FreeTeam.Events
{
    public interface IEventDispatcher
    {
        void AddListener<T> (EventHandler _listener) where T : Event;
        void RemoveListener<T> (EventHandler _listener) where T : Event;
        void Dispatch (Event _event);
    }
}