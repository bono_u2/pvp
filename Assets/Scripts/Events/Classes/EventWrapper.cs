﻿namespace FreeTeam.Events
{
    public class EventWrapper
    {
        #region Vars
        public event EventHandler Handler;
        #endregion

        #region Public methods
        public void Invoke (Event _event)
        {
            Handler?.Invoke (_event);
        }
        #endregion
    }
}