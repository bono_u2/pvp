﻿using System.Collections.Generic;

namespace FreeTeam.Events
{
    public class EventDispatcher : IEventDispatcher
    {
        #region Vars
        private Dictionary<System.Type, EventWrapper> wrappers;
        #endregion

        public EventDispatcher ()
        {
            wrappers = new Dictionary<System.Type, EventWrapper> ();
        }

        #region Public methods
        public void AddListener<T> (EventHandler _listener) where T : Event
        {
            var type = typeof (T);
            if (!wrappers.TryGetValue (type, out EventWrapper eventWrapper))
            {
                eventWrapper = new EventWrapper ();
                eventWrapper.Handler += _listener;
                wrappers.Add (type, eventWrapper);
            }
            else
                eventWrapper.Handler += _listener;
        }

        public void RemoveListener<T> (EventHandler _listener) where T : Event
        {
            var type = typeof (T);
            if (wrappers.TryGetValue (type, out EventWrapper thisEvent))
                thisEvent.Handler -= _listener;
        }

        public void Dispatch (Event _event)
        {

            if (wrappers.TryGetValue (_event.GetType (), out EventWrapper eventWrapper))
                eventWrapper.Invoke (_event);
        }
        #endregion
    }
}