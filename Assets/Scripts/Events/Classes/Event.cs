﻿namespace FreeTeam.Events
{
    public class Event : IEvent
    {
        public Event (object _sender, string _type)
        {
            Sender = _sender;
            Type = _type;
        }

        #region Get / Set
        public object Sender { get; private set; }
        public string Type { get; private set; }
        #endregion
    }
}