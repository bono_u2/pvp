﻿using Slash.Unity.DataBind.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using TMPro;
using UnityEngine;

public class SettingsDialogController : DialogController
{
    #region Vars
    public TMP_Dropdown langDropdown;

    private Dictionary<SystemLanguage, string> langs = new Dictionary<SystemLanguage, string> ();
    #endregion

    #region Public methods
    public void OnSetResolutionBtnHabdler (int _idx)
    {
        var resolutions = SettingsManager.GetAvailableResolutions ();

        SettingsManager.Instance.settings.GraphicsSettings.Resolution = resolutions[_idx];
        SettingsManager.Instance.Apply ();
        SettingsManager.Instance.Save ();

        SettingsManager.Instance.ApplyResolution ();
    }

    public void OnFPSLimitBtnHandler (bool _isLimited)
    {
        SettingsManager.Instance.settings.GraphicsSettings.TargetFrameRate = (_isLimited) ? GraphicsSettings.TARGET_FRAMERATE_MIN : GraphicsSettings.TARGET_FRAMERATE_UNLIM;
        SettingsManager.Instance.Apply ();
        SettingsManager.Instance.Save ();
    }

    public void OnLanguageChangedHandler ()
    {
        var lang = (SystemLanguage)(langDropdown.options[langDropdown.value] as TMP_OptionDataObject).value;

        SettingsManager.Instance.settings.GameSettings.Language = lang;
        SettingsManager.Instance.Apply ();
        SettingsManager.Instance.Save ();

        UpdateLangDropdown ();
    }

    public void OnErrorBtnHandler ()
    {
        throw new Exception ("Error!");
    }

    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        langs.Add (SystemLanguage.English, "lang_en");
        langs.Add (SystemLanguage.Russian, "lang_ru");

        UpdateLangDropdown ();
    }
    #endregion

    #region Private methods
    private void UpdateLangDropdown ()
    {
        var currentLang = LocalizationManager.CurrentLanguage;
        var keys = langs.Keys.ToList ();

        langDropdown.options.Clear ();
        for (var i = 0; i < keys.Count; i++)
        {
            var key = keys[i];
            var str = LocalizationManager.Get (langs[key]);
            langDropdown.options.Add (new TMP_OptionDataObject () { text = str, value = key });
            if (key == currentLang)
                langDropdown.value = i;
        }
        langDropdown.RefreshShownValue ();
    }
    #endregion
}

public class SettingsDialogContext : Context
{
    #region Properties
    private readonly Property<string> TextProperty = new Property<string> ();
    public string Text
    {
        get { return TextProperty.Value; }
        set { TextProperty.Value = value; }
    }
    #endregion
}