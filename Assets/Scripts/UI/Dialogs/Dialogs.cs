﻿using System;

public class Dialogs
{
    #region Constants
    public const string TEST_FIRST_DIALOG_NAME = "TestFirstDialog/TestFirstDialog";
    public const string TEST_SECOND_DIALOG_NAME = "TestSecondDialog/TestSecondDialog";

    public const string ERROR_DIALOG_NAME = "ErrorDialog/ErrorDialog";
    public const string EXIT_GAME_DIALOG_NAME = "ExitGameDialog/ExitGameDialog";
    public const string LOGGER_DIALOG_NAME = "LoggerDialog/LoggerDialog";

    public const string SETTINGS_DIALOG_NAME = "SettingsDialog/SettingsDialog";

    public const string PAUSE_BATTLE_DIALOG_NAME = "PauseBattleDialog/PauseBattleDialog";
    #endregion
}