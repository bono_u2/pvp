﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;

public class ErrorDialogController : DialogController
{
    #region Vars
    private ErrorDialogContext context;
    #endregion

    #region Public methods
    public void OnSwitchViewBtnHandler ()
    {
        context.IsStacktraceVisible = !context.IsStacktraceVisible;
    }

    public void OnCopyStackBtnHandler ()
    {
        UniClipboard.SetText (context.TechInfo);
    }

    public void OnOpenLoggerDialogBtnHandler ()
    {
        DialogsManager.CreateDialog<LoggerDialogController> (Dialogs.LOGGER_DIALOG_NAME);
    }

    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        string message = Data[0] as string;
        string techInfo = Data[1] as string;

        context = GetComponent<ContextHolder> ().Context as ErrorDialogContext;
        context.Message = message ?? "";
        context.TechInfo = techInfo ?? "";
        context.IsStacktraceVisible = false;
    }
    #endregion
}

public class ErrorDialogContext : Context
{
    #region Properties
    private readonly Property<string> MessageProperty = new Property<string> ();
    public string Message
    {
        get { return MessageProperty.Value; }
        set { MessageProperty.Value = value; }
    }

    private readonly Property<string> TechInfoProperty = new Property<string> ();
    public string TechInfo
    {
        get { return TechInfoProperty.Value; }
        set { TechInfoProperty.Value = value; }
    }

    private readonly Property<bool> IsStacktraceVisibleProperty = new Property<bool> ();
    public bool IsStacktraceVisible
    {
        get { return IsStacktraceVisibleProperty.Value; }
        set { IsStacktraceVisibleProperty.Value = value; }
    }
    #endregion
}