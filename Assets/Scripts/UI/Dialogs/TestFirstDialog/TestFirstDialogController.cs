﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class TestFirstDialogController : DialogController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnOpenTestSecondDialogBtnHandler ()
    {
        Hide ();

        var data = "Message to TestSecondDialog...";
        var testSecondDialog = DialogsManager.CreateDialog<TestSecondDialogController> ("TestSecondDialog/TestSecondDialog", data);
        testSecondDialog.AddListener (DialogEventType.Close, OnTestSecondDialogCloseHandler);
    }

    public void OnCloseBtnHandler ()
    {
        Result = true;

        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        Console.Message ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Result Type: {Data.GetType ().ToString ()}, Value = <i>{Data.ToString ()}</i></color>", ConsoleChannelType.Debug);
    }

    protected override void Destroy ()
    {
        Console.Log ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Is closed!</color>", ConsoleChannelType.Debug);
    }
    #endregion

    #region Private methods
    private void OnTestSecondDialogCloseHandler (DialogEvent _event)
    {
        var testSecondDialog = _event.Sender;
        testSecondDialog.RemoveListener (DialogEventType.Close, OnTestSecondDialogCloseHandler);

        var result = testSecondDialog.Result;
        if (result != null)
            Console.Message ($"<color=#dddddd><b>(TestFirstDialogController)</b> Sender: {_event.Sender.GetType ().ToString ()}, Result Type: {result.GetType ().ToString ()}, Value = <i>{result.ToString ()}</i></color>", ConsoleChannelType.Debug);
        else
            Console.Warning ("<color=#dddddd><b>(TestFirstDialogController)</b> Result is null!</color>", ConsoleChannelType.Debug);

        Show ();
    }
    #endregion
}