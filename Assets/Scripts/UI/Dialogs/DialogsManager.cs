﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogsManager : MonoBehaviour
{
    #region Singleton
    private static DialogsManager instance_;
    private static DialogsManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<DialogsManager> ();

            if (instance_ == null)
                Console.Error ($"No <b>{typeof (DialogsManager).FullName}</b> instance set");

            return instance_;
        }
    }
    #endregion

    #region Constants
    private const string DIALOG_PREFABS_PATH = "Ui/Dialogs/{0}";

    private const string INITIALIZATION_METHOD_NAME = "Initialization";
    private const string DESTROY_METHOD_NAME = "Destroy";
    private const string KEY_EVENT_METHOD_NAME = "KeyEvent";
    #endregion

    #region Vars
    private List<DialogController> dialogs = new List<DialogController> ();
    #endregion

    #region Unity methods
    void OnGUI ()
    {
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyUp)
        {
            var dialogController = dialogs.LastOrDefault ();
            if (dialogController)
                dialogController.SendMessage (KEY_EVENT_METHOD_NAME, e.keyCode);
        }
    }
    #endregion

    #region Private methods
    private DialogController Create (string _dialogName, params object[] _data)
    {
        return Create<DialogController> (_dialogName);
    }

    private T Create<T> (string _dialogName, params object[] _data) where T : DialogController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_dialogName))
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) DialogName is null or empty!", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (DIALOG_PREFABS_PATH, _dialogName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Prefab \"<b>{_dialogName}</b>\" no found!", ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var dialogGO = Instantiate (prefab, this.transform, false);

        // Get DialogController component
        var dialogController = dialogGO.GetComponent<T> ();
        if (!dialogController)
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Controller \"<b>{typeof (T)}</b>\" in prefab \"<b>{_dialogName}</b>\" no found!", ConsoleChannelType.UI);

            // Destroy dialog instance
            dialogGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        dialogs.Add (dialogController);

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) Dialogs count: <b>{dialogs.Count}</b>", ConsoleChannelType.UI);

        // set dialog Data
        dialogController.Data = _data;

        // подписываемся на событие закрытия диалога
        dialogController.AddListener (DialogEventType.Destruction, OnDestructionDialogHandler);

        // Init DialogController
        dialogController.SendMessage (INITIALIZATION_METHOD_NAME);

        return dialogController;
    }

    private void Close ()
    {
        var dialogController = dialogs.Last ();

        Close (dialogController);
    }

    private void Close (DialogController _dialogController)
    {
        if (!_dialogController)
            return;

        _dialogController.Close ();
    }

    private void Destroy ()
    {
        // Last DialogController
        var dialogController = dialogs.LastOrDefault ();

        // Destroy dialog
        Destroy (dialogController);
    }

    private void Destroy (DialogController _dialogController)
    {
        // Check DialogController
        if (!_dialogController)
            return;

        // Call Closing DialogController
        _dialogController.SendMessage (DESTROY_METHOD_NAME);

        // Remove DialogController from dialogs list
        dialogs.Remove (_dialogController);

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) Dialogs count: <b>{dialogs.Count}</b>", ConsoleChannelType.UI);

        // Destroy dialog GameObject
        GameObject.Destroy (_dialogController.gameObject);
    }

    private void DestroyAll ()
    {
        for (var i = 0; i < dialogs.Count; i++)
            Destroy (dialogs[i]);
    }

    private void OnDestructionDialogHandler (UIEvent _event)
    {
        var e = (DialogEvent)_event;
        var dialogController = e.Sender;

        // отписываемся от события
        dialogController.RemoveListener (DialogEventType.Destruction, OnDestructionDialogHandler);

        // уничтожаем диалог
        Destroy (dialogController);
    }
    #endregion

    #region Public static methods
    public static DialogController CreateDialog (string _dialogName, params object[] _data)
    {
        return Instance.Create (_dialogName, _data);
    }

    public static T CreateDialog<T> (string _dialogName, params object[] _data) where T : DialogController
    {
        return Instance.Create<T> (_dialogName, _data);
    }

    public static void CloseDialog ()
    {
        Instance.Close ();
    }

    public static void CloseDialog (DialogController _dialogController)
    {
        Instance.Close (_dialogController);
    }

    public static void DestroyDialog ()
    {
        Instance.Destroy ();
    }

    public static void DestroyDialog (DialogController _dialogController)
    {
        Instance.Destroy (_dialogController);
    }

    public static void DestroyAllDialogs ()
    {
        Instance.DestroyAll ();
    }

    public static int DialogsCount { get { return Instance.dialogs.Count; } }
    #endregion
}