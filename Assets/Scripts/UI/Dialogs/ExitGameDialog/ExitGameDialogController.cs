﻿public class ExitGameDialogController : DialogController
{
    #region Public methods
    public void OnCancelBtnHandler ()
    {
        Result = false;

        Close ();
    }

    public void OnExitBtnHandler ()
    {
        Result = true;

        Close ();
    }
    #endregion
}