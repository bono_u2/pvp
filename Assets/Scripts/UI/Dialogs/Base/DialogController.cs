﻿using System.Collections;
using UnityEngine;

public abstract class DialogController : UIController<DialogEvent>
{
    #region Vars
    private Animator animator_;
    private CanvasGroup canvasGroup_;
    #endregion

    #region Get / Set
    public bool IsInited { get; protected set; } = false;
    public bool IsRunning { get; private set; } = false;
    public bool IsShowed { get; private set; } = false;

    public object[] Data { get; set; }
    public object Result { get; protected set; }

    protected Animator Animator
    {
        get
        {
            if (!animator_)
            {
                animator_ = this.gameObject.GetComponent<Animator> ();
                if (!animator_)
                    animator_ = this.gameObject.AddComponent<Animator> ();
            }

            return animator_;
        }
    }

    protected CanvasGroup CanvasGroup
    {
        get
        {
            if (!canvasGroup_)
            {
                canvasGroup_ = this.gameObject.GetComponent<CanvasGroup> ();
                if (!canvasGroup_)
                    canvasGroup_ = this.gameObject.AddComponent<CanvasGroup> ();
            }

            return canvasGroup_;
        }
    }
    #endregion

    #region Public methods
    public sealed override void Close ()
    {
        Dispatch (DialogEventType.Close);

        StartCoroutine (Closing ());
    }

    public void Show ()
    {
        StartCoroutine (Showing ());
    }

    public void Hide ()
    {
        StartCoroutine (Hiding ());
    }
    #endregion

    #region Protected methods
    protected virtual void Init ()
    {
    }

    protected virtual void Destroy ()
    {
    }

    protected virtual void KeyEvent (KeyCode _keyCode)
    {
        if (_keyCode == KeyCode.Escape && IsInited && IsRunning && IsShowed)
            Close ();
    }
    #endregion

    #region Private methods
    private void Initialization ()
    {
        if (IsInited)
            return;

        Init ();

        IsInited = true;
        IsRunning = true;

        Dispatch (DialogEventType.Init);

        Show ();
    }

    private void Destruction ()
    {
        Dispatch (DialogEventType.Destruction);

        IsRunning = false;
    }
    #endregion

    #region Coroutines
    private IEnumerator Showing ()
    {
        CanvasGroup.alpha = 1.0f; 
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;

        if (Animator && !IsShowed)
        {
            Animator.SetBool ("showed", true);

            yield return new WaitForEndOfFrame ();

            var duration = 0.0f;
            var layerIdx = Animator.GetLayerIndex ("Base Layer");
            var clipInfo = Animator.GetCurrentAnimatorClipInfo (layerIdx);
            if (clipInfo != null && clipInfo.Length > 0)
            {
                var clip = clipInfo[0].clip;
                if (clip != null)
                {
                    //Console.Info ($"name: {clip.name}, length: {clip.length}");
                    duration = clip.length;
                }
            }
            else
                Console.Warning ("ClipInfo not found!");

            yield return new WaitForSecondsRealtime (duration);
        }

        yield return null;

        IsShowed = true;

        Dispatch (DialogEventType.Show);
    }

    private IEnumerator Hiding ()
    {
        if (Animator && IsShowed)
        {
            Animator.SetBool ("showed", false);

            yield return new WaitForEndOfFrame ();

            var duration = 0.0f;
            var layerIdx = Animator.GetLayerIndex ("Base Layer");
            var clipInfo = Animator.GetCurrentAnimatorClipInfo (layerIdx);
            if (clipInfo != null && clipInfo.Length > 0)
            {
                var clip = clipInfo[0].clip;
                if (clip != null)
                {
                    //Console.Info ($"name: {clip.name}, length: {clip.length}");
                    duration = clip.length;
                }
            }
            else
                Console.Warning ("ClipInfo not found!");

            yield return new WaitForSecondsRealtime (duration);
        }

        yield return null;

        IsShowed = false;

        CanvasGroup.alpha = 0.0f;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;

        Dispatch (DialogEventType.Hide);
    }

    private IEnumerator Closing ()
    {
        yield return Hiding ();

        Destruction ();
    }

    private IEnumerator WaitForAnimation (Animation animation)
    {
        do
        {
            yield return null;
        } while (animation.isPlaying);
    }

    public IEnumerator WaitShowing ()
    {
        while (!IsShowed)
            yield return null;
    }

    public IEnumerator WaitHiding ()
    {
        while (IsShowed)
            yield return null;
    }

    public IEnumerator Wait ()
    {
        while (IsRunning)
            yield return null;
    }
    #endregion
}