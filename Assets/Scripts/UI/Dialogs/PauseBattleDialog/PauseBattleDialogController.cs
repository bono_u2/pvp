﻿public class PauseBattleDialogController : DialogController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnResumeBtnHandler ()
    {
        Result = false;

        Close ();
    }

    public void OnAbortBtnHandler ()
    {
        Result = true;

        Close ();
    }
    #endregion
}