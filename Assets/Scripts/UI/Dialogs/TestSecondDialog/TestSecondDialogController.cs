﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class TestSecondDialogController : DialogController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnCloseBtnHandler ()
    {
        Result = "Result message TestSecondDialog!";

        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        Console.Message ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Result Type: {Data.GetType ().ToString ()}, Value = <i>{Data.ToString ()}</i></color>", ConsoleChannelType.Debug);
    }

    protected override void Destroy ()
    {
        Console.Log ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Is closed!</color>", ConsoleChannelType.Debug);
    }
    #endregion
}