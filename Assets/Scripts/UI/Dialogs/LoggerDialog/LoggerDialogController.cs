﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;
using System.Collections.Generic;
using System.Linq;
using System;
using TMPro;
using UnityEngine.UI;

public class LoggerDialogController : DialogController
{
    #region Vars
    public TMP_Dropdown channlesDropdown;
    public ScrollRect scrollRect;

    private LoggerDialogContext context;

    private int logChannelIdx = -1;
    #endregion

    #region Get / Set
    public ConsoleLogType LogTypeFlag { get; private set; }
    #endregion

    #region Public methods
    public void OnLogChannelChanged ()
    {
        logChannelIdx = (channlesDropdown.options[channlesDropdown.value] as TMP_OptionDataInt).value;

        UpdateLogList ();

        if (scrollRect)
            scrollRect.verticalNormalizedPosition = 0f;
    }

    public void OnShowHideLogBtnHandler ()
    {
        UpdateLogTypeFlag (ConsoleLogType.Log);
        UpdateLogList ();
    }

    public void OnShowHideInfoBtnHandler ()
    {
        UpdateLogTypeFlag (ConsoleLogType.Info);
        UpdateLogList ();
    }

    public void OnShowHideMessageBtnHandler ()
    {
        UpdateLogTypeFlag (ConsoleLogType.Message);
        UpdateLogList ();
    }

    public void OnShowHideWarningBtnHandler ()
    {
        UpdateLogTypeFlag (ConsoleLogType.Warning);
        UpdateLogList ();
    }

    public void OnShowHideErrorBtnHandler ()
    {
        UpdateLogTypeFlag (ConsoleLogType.Error);
        UpdateLogList ();
    }

    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as LoggerDialogContext;
        context.LogTypeFlag = ConsoleLogType.Log | ConsoleLogType.Info | ConsoleLogType.Message | ConsoleLogType.Warning | ConsoleLogType.Error;

        UpdateLogChannelsDropDown ();
        UpdateLogList ();

        if (scrollRect)
            scrollRect.verticalNormalizedPosition = 0f;
    }
    #endregion

    #region Private methods
    private void UpdateLogChannelsDropDown ()
    {
        var channels = Enum.GetValues (typeof (ConsoleChannelType)).Cast<ConsoleChannelType> ().ToList ();
        channlesDropdown.options.Clear ();
        channlesDropdown.options.Add (new TMP_OptionDataInt () { text = "All channels", value = -1 });
        foreach (var item in channels)
            channlesDropdown.options.Add (new TMP_OptionDataInt () { text = item.ConvertToString (), value = (int)item });
        channlesDropdown.value = 0;
        channlesDropdown.RefreshShownValue ();
    }

    private void UpdateLogList ()
    {
        var fullLog = Console.GetFullConsoleLog ();
        var log = ((logChannelIdx < 0) ? fullLog : fullLog.FindAll (x => (int)x.Channel == logChannelIdx)).FindAll (x => ((context.LogTypeFlag & x.LogType) == x.LogType));

        context.Items.Clear ();

        for (var i = 0; i < log.Count; i++)
        {
            var logItem = log[i];
            context.Items.Add (new LoggerDialogItemContext (logItem));
            if (i < log.Count - 1)
                context.Items.Add (new LoggerDialogItemContext ("-"));
        }
    }

    private void UpdateLogTypeFlag (ConsoleLogType _consoleLogType)
    {
        if ((context.LogTypeFlag & _consoleLogType) == _consoleLogType)
            context.LogTypeFlag &= ~_consoleLogType;
        else
            context.LogTypeFlag |= _consoleLogType;
    }
    #endregion
}

public class LoggerDialogContext : Context
{
    #region Collections
    private readonly Collection<LoggerDialogItemContext> ItemsCollection = new Collection<LoggerDialogItemContext> ();
    public Collection<LoggerDialogItemContext> Items
    {
        get { return ItemsCollection; }
    }
    #endregion

    #region Properties
    private readonly Property<ConsoleLogType> LogTypeFlagProperty = new Property<ConsoleLogType> ();
    public ConsoleLogType LogTypeFlag
    {
        get { return LogTypeFlagProperty.Value; }
        set { LogTypeFlagProperty.Value = value; }
    }
    #endregion
}

public class LoggerDialogItemContext : Context
{
    #region Static vars
    static string LOG_PREFIX        = "<color=orange><b>»</b></color> <color=#eeeeee><b>[Log]</b></color> ";
    static string INFO_PREFIX       = "<color=orange><b>»</b></color> <color=#5998ff><b>[Info]</b></color> ";
    static string MESSAGE_PREFIX    = "<color=orange><b>»</b></color> <color=#99BB33><b>[Message]</b></color> ";
    static string WARNING_PREFIX    = "<color=orange><b>»</b></color> <color=#ffcb49><b>[Warning]</b></color> ";
    static string ERROR_PREFIX      = "<color=orange><b>»</b></color> <color=#b50707><b>[Error]</b></color> ";

    static Dictionary<ConsoleLogType, string> PREFIXES = new Dictionary<ConsoleLogType, string> () {
                { ConsoleLogType.Log, LOG_PREFIX},
                { ConsoleLogType.Info, INFO_PREFIX},
                { ConsoleLogType.Message, MESSAGE_PREFIX},
                { ConsoleLogType.Warning, WARNING_PREFIX},
                { ConsoleLogType.Error, ERROR_PREFIX},
            };
    #endregion

    #region Properties
    private readonly Property<ConsoleLogType> LogTypeProperty = new Property<ConsoleLogType> ();
    public ConsoleLogType LogType
    {
        get { return LogTypeProperty.Value; }
        set { LogTypeProperty.Value = value; }
    }

    private readonly Property<string> TextProperty = new Property<string> ();
    public string Text
    {
        get { return PREFIXES[LogType] + TextProperty.Value; }
        set { TextProperty.Value = value; }
    }

    private readonly Property<string> DateProperty = new Property<string> ();
    public string Date
    {
        get { return DateProperty.Value; }
        set { DateProperty.Value = value; }
    }

    private readonly Property<bool> IsSeparatorProperty = new Property<bool> ();
    public bool IsSeparator
    {
        get { return IsSeparatorProperty.Value; }
        set { IsSeparatorProperty.Value = value; }
    }
    #endregion

    public LoggerDialogItemContext (ConsoleLog _consoleLog)
    {
        LogType = _consoleLog.LogType;
        Text = _consoleLog.Text;
        Date = string.Format ("[{0}]", _consoleLog.Date.ToString ("HH:mm:ss"));
        IsSeparator = _consoleLog.Text == "-";
    }

    public LoggerDialogItemContext (string _text, string _date = "")
    {
        LogType = ConsoleLogType.Log;
        Text = _text;
        Date = _date;
        IsSeparator = _text == "-";
    }

    public LoggerDialogItemContext (ConsoleLogType _logType, string _text = "", string _date = "")
    {
        LogType = _logType;
        Text = _text;
        Date = _date;
        IsSeparator = _text == "-";
    }
}