﻿using TMPro;
using UnityEngine.UI;
using UnityEngine;

[ExecuteInEditMode]
public class ProgressBar : MonoBehaviour
{
    #region Vars
    public Image progressImage;
    public TextMeshProUGUI valueText;

    public float minValue = 0.0f;
    public float maxValue = 1.0f;

    public string valueFormatString = "F1";

    [Range (0.0f, 1.0f)]
    public float percent = 0.5f;
    private float percentPrev = -1;
    #endregion

    #region Unity methods
    void OnValidate ()
    {
        RefreshGUI ();
    }

    void Update ()
    {
        if (percentPrev == percent)
            return;

        RefreshGUI ();
    }
    #endregion

    #region Get / Set
    public float Value
    {
        get { return Mathf.Lerp (minValue, maxValue, percent); }    //a + (b - a) * t
        set
        {
            var v = Mathf.Clamp (value, minValue, maxValue);
            percent = (v - minValue) / (maxValue - minValue);
        }
    }
    #endregion

    #region Protected methods
    protected virtual void RefreshGUI ()
    {
        if (progressImage)
            progressImage.fillAmount = percent;

        if (valueText)
            valueText.text = Value.ToString (valueFormatString);

        percentPrev = percent;
    }
    #endregion
}