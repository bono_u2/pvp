﻿using Slash.Unity.DataBind.Foundation.Setters;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] ProgressBar Value Setter")]
public class ProgressBarValueSetter : ComponentSingleSetter<ProgressBar, float>
{
    protected override void OnValueChanged (float _newValue)
    {
        this.Target.Value = _newValue;
    }
}