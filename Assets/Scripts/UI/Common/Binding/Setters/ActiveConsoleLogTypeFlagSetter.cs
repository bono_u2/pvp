﻿using Slash.Unity.DataBind.Foundation.Setters;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] Active ConsoleLogType Flag Setter")]
public class ActiveConsoleLogTypeFlagSetter : GameObjectMultiSetter<ConsoleLogType>
{
    #region Vars
    public List<ActiveConsoleLogTypeFlagSetterItem> Targets = new List<ActiveConsoleLogTypeFlagSetterItem> ();
    #endregion

    #region Methods
    protected override void OnValueChanged (ConsoleLogType newValue)
    {
        foreach (var item in Targets)
        {
            var valueBool = (newValue & item.enumValue) == item.enumValue;
            item.target.SetActive (item.inverted ? !valueBool : valueBool);
        }
    }
    #endregion
}

[Serializable]
public class ActiveConsoleLogTypeFlagSetterItem
{
    #region Vars
    public ConsoleLogType enumValue = ConsoleLogType.Log;
    public GameObject target = null;
    public bool inverted = false;
    #endregion
}