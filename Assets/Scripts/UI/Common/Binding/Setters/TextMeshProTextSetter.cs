﻿using Slash.Unity.DataBind.Foundation.Setters;
using TMPro;
using UnityEngine;

[AddComponentMenu ("Data Bind/UnityUI/Setters/[DB] TMP Text Setter")]
public class TextMeshProTextSetter : ComponentSingleSetter<TextMeshProUGUI, string>
{
    #region Methods
    protected override void OnValueChanged (string _newValue)
    {
        var target = this.Target;
        if (target != null)
            target.text = _newValue;
    }
    #endregion
}