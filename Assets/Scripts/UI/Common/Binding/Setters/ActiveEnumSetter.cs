﻿using Slash.Unity.DataBind.Foundation.Setters;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] Active Enum Setter")]
public class ActiveEnumSetter : GameObjectSingleSetter<Enum>
{
    #region Vars
    public List<EnumActiveSetterItem> Targets = new List<EnumActiveSetterItem> ();
    #endregion

    #region Methods
    protected override void OnValueChanged (Enum newValue)
    {
        var newValueStr = newValue.ToString ();
        foreach (var item in Targets)
        {
            var valueBool = (item.enumValue == newValueStr);
            item.target.SetActive (item.inverted ? !valueBool : valueBool);
        }
    }
    #endregion
}

[Serializable]
public class EnumActiveSetterItem
{
    #region Vars
    public string enumValue;
    public GameObject target;
    public bool inverted;
    #endregion
}