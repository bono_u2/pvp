﻿using Slash.Unity.DataBind.Foundation.Setters;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] Active Setter with invert")]
public class ActiveWithInvertSetter : ActiveSetter
{
    #region Vars
    public bool inverted;
    #endregion

    #region Methods
    protected override void OnValueChanged (bool newValue)
    {
        this.Target.SetActive (inverted ? !newValue : newValue);
    }
    #endregion
}