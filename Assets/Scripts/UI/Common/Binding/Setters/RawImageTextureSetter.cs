﻿using Slash.Unity.DataBind.Foundation.Setters;
using UnityEngine.UI;
using UnityEngine;

[AddComponentMenu ("Data Bind/UnityUI/Setters/[DB] RawImage Texture Setter")]
public class RawImageTextureSetter : ComponentSingleSetter<RawImage, Texture>
{
    #region Methods

    protected override void OnValueChanged (Texture newValue)
    {
        this.Target.texture = newValue;
    }

    #endregion
}