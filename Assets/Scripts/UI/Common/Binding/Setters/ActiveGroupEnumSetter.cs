﻿using Slash.Unity.DataBind.Foundation.Setters;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] Active Group Enum Setter")]
public class ActiveGroupEnumSetter : GameObjectSingleSetter<Enum>
{
    #region Vars
    public List<ActiveGroupEnumSetterItem> targets = new List<ActiveGroupEnumSetterItem> ();
    #endregion

    #region Methods
    protected override void OnValueChanged (Enum _newValue)
    {
        bool valueBool;
        foreach (var item in targets)
        {
            valueBool = (item.enumValue == _newValue.ToString ());

            foreach (var subItem in item.targets)
                subItem.target.SetActive (subItem.inverted ? !valueBool : valueBool);
        }
    }
    #endregion
}

[Serializable]
public class ActiveGroupEnumSetterItem
{
    #region Vars
    public string enumValue;
    public List<ActiveGroupGameObjectSetterItem> targets = new List<ActiveGroupGameObjectSetterItem> ();
    #endregion
}

[Serializable] 
public class ActiveGroupGameObjectSetterItem
{
    #region Vars
    public GameObject target;
    public bool inverted;
    #endregion
}