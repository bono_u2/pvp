﻿using Slash.Unity.DataBind.Foundation.Setters;
using System.Collections.Generic;
using System;
using UnityEngine;

[AddComponentMenu ("Data Bind/Foundation/Setters/[DB] Active Group Setter")]
public class ActiveGroupSetter : GameObjectMultiSetter<bool>
{
    #region Vars
    public List<GroupActiveSetterItem> Targets = new List<GroupActiveSetterItem> ();
    #endregion

    #region Methods
    protected override void OnValueChanged (bool _newValue)
    {
        this.Targets.ForEach (x => x.target.SetActive (x.inverted ? !_newValue : _newValue));
    }
    #endregion
}

[Serializable]
public class GroupActiveSetterItem
{
    #region Vars
    public GameObject target;
    public bool inverted;
    #endregion
}