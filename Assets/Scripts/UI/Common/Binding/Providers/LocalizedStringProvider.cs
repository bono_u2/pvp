﻿using Slash.Unity.DataBind.Core.Presentation;
using UnityEngine;

public class LocalizedStringProvider : DataProvider
{
    #region Vars
    // The name of the phrase we want to use
    [SerializeField]
    public string PhraseName;
    #endregion

    #region Get / Set
    public override object Value
    {
        get
        {
            return (string.IsNullOrEmpty (PhraseName)) ? "" : LocalizationManager.Get (PhraseName);
        }
    }
    #endregion

    #region Unity methods
    protected override void OnEnable ()
    {
        base.OnEnable ();

        LocalizationManager.Instance.AddListener<LanguageChangedEvent> (LocalizationChanged);

        UpdateValue ();
    }

    protected override void OnDisable ()
    {
        base.OnDisable ();

        LocalizationManager.Instance.RemoveListener<LanguageChangedEvent> (LocalizationChanged);
    }
    #endregion

    #region Protected methods
    protected override void UpdateValue ()
    {
        this.OnValueChanged (this.Value);
    }
    #endregion

    #region Private methods
    private void LocalizationChanged (FreeTeam.Events.Event _event)
    {
        UpdateValue ();
    }
    #endregion
}