﻿using System;

public class UIEvent
{
    public UIEvent (object _sender, Enum _type, object _data = null)
    {
        Sender = _sender;
        Type = _type;
        Data = _data;
    }

    #region Get / Set
    public object Sender { get; private set; }
    public Enum Type { get; private set; }
    public object Data { get; private set; }
    #endregion
}