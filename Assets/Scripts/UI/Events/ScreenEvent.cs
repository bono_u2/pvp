﻿using System;

public class ScreenEvent : UIEvent
{
    public ScreenEvent (object _sender, Enum _type, object _data = null) : base (_sender, _type, _data) { }

    #region Get / Set
    public new ScreenController Sender
    {
        get { return (ScreenController)base.Sender; }
    }

    public new ScreenEventType Type
    {
        get { return (ScreenEventType)base.Type; }
    }
    #endregion
}