﻿using System;

public class DialogEvent : UIEvent
{
    public DialogEvent (object _sender, Enum _type, object _data = null) : base (_sender, _type, _data)
    {
    }

    #region Get / Set

    public new DialogController Sender
    {
        get { return (DialogController)base.Sender; }
    }

    public new DialogEventType Type
    {
        get { return (DialogEventType)base.Type; }
    }

    #endregion Get / Set
}