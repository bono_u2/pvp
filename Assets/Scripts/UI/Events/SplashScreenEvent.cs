﻿using System;

public class SplashScreenEvent : UIEvent
{
    public SplashScreenEvent (object _sender, Enum _type, object _data = null) : base (_sender, _type, _data) { }

    #region Get / Set
    public new SplashScreenController Sender
    {
        get { return (SplashScreenController)base.Sender; }
    }

    public new SplashScreenEventType Type
    {
        get { return (SplashScreenEventType)base.Type; }
    }
    #endregion
}