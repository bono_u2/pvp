﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SplashScreensManager : MonoBehaviour
{
    #region Singleton
    private static SplashScreensManager instance_;
    private static SplashScreensManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<SplashScreensManager> ();

            if (instance_ == null)
                Console.Error ($"No <b>{typeof (SplashScreensManager).FullName}</b> instance set");

            return instance_;
        }
    }
    #endregion

    #region Static vars
    private const string SPLASH_SCREEN_PREFABS_PATH = "Ui/SplashScreens/{0}";
    #endregion

    #region Vars
    private List<SplashScreenController> splashScreens = new List<SplashScreenController> ();
    #endregion

    #region Get / Set
    public SplashScreenController Current { get { return splashScreens.LastOrDefault (); } }
    #endregion

    #region Private methods
    private SplashScreenController Create (string _splashScreenName, object _data = null, bool _keepHistory = true)
    {
        return Create<SplashScreenController> (_splashScreenName, _data, _keepHistory);
    }

    private T Create<T> (string _splashScreenName, object _data = null, bool _keepHistory = true) where T : SplashScreenController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_splashScreenName))
        {
            // Debug
            Console.Error ("<color=#eeeeee>(SplashScreensManager) SplashScreenName is null or empty!</color>", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (SPLASH_SCREEN_PREFABS_PATH, _splashScreenName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Prefab \"<b>{_splashScreenName}</b>\" no found!", ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var splashScreenGO = Instantiate (prefab, transform, false);

        // Get DialogController component
        var splashScreenController = splashScreenGO.GetComponent<T> ();
        if (!splashScreenController)
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Controller \"<b>{typeof (T)}</b>\" in prefab \"<b>{_splashScreenName}</b>\" no found!", ConsoleChannelType.UI);

            // Destroy screen instance
            splashScreenGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        splashScreens.Add (splashScreenController);

        // set dialog Data
        splashScreenController.Data = _data;

        // подписываемся на событие закрытия диалога
        splashScreenController.AddListener (SplashScreenEventType.Destruction, OnDestructionSplashScreenHandler);

        // Init DialogController
        splashScreenController.SendMessage ("Initialization");

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) SplashScreens count: <b>{splashScreens.Count}</b>", ConsoleChannelType.UI);

        return splashScreenController;
    }

    private void Destroy ()
    {
        // Last DialogController
        var splashScreenController = splashScreens.LastOrDefault ();

        // Destroy splashScreen
        Destroy (splashScreenController);
    }

    private void Destroy (SplashScreenController _splashScreenController)
    {
        // Check DialogController
        if (!_splashScreenController)
        {
            Console.Error ($"({this.GetType ().ToString ()}) Controller is null!", ConsoleChannelType.UI);
            return;
        }

        // отписываемся от события
        _splashScreenController.RemoveListener (SplashScreenEventType.Destruction, OnDestructionSplashScreenHandler);

        // Call Closing SplashScreenController
        _splashScreenController.SendMessage ("Destroy");

        // Remove SplashScreenController from dialogs list
        splashScreens.Remove (_splashScreenController);

        // Destroy SplashScreen GameObject
        GameObject.Destroy (_splashScreenController.gameObject);

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) SplashScreens count: <b>{splashScreens.Count}</b>", ConsoleChannelType.UI);
    }

    private void DestroyAll ()
    {
        for (var i = 0; i < splashScreens.Count; i++)
            Destroy (splashScreens[i]);
    }

    private void OnDestructionSplashScreenHandler (SplashScreenEvent _event)
    {
        // уничтожаем диалог
        Destroy (_event.Sender);
    }
    #endregion

    #region Public static methods
    public static SplashScreenController CreateSplashScreen (string _screenName, object _data = null)
    {
        return SplashScreensManager.Instance.Create (_screenName, _data);
    }

    public static T CreateSplashScreen<T> (string _screenName, object _data = null) where T : SplashScreenController
    {
        return SplashScreensManager.Instance.Create<T> (_screenName, _data);
    }

    public static void DestroySplashScreen ()
    {
        SplashScreensManager.Instance.Destroy ();
    }

    public static void DestroySplashScreen (SplashScreenController _screenController)
    {
        SplashScreensManager.Instance.Destroy (_screenController);
    }

    public static void DestroyAllSplashScreeens ()
    {
        SplashScreensManager.Instance.DestroyAll ();
    }
    #endregion
}