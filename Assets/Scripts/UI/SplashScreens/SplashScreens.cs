﻿public class SplashScreens
{
    #region Constants
    public const string LOAD_GAME_SPLASH_SCREEN_NAME = "LoadGameSplashScreen/LoadGameSplashScreen";
    public const string LOAD_BATTLE_SPLASH_SCREEN_NAME = "LoadBattleSplashScreen/LoadBattleSplashScreen";
    #endregion
}