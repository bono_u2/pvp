﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;

public class LoadBattleSplashScreenController : SplashScreenController
{
    #region Vars
    private LoadBattleSplashScreenContext context;
    #endregion

    #region Get / Set
    // reserved
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as LoadBattleSplashScreenContext;

        UpdateUI ();
    }
    #endregion

    #region Private methods
    private void UpdateUI ()
    {
        context.Title = LocalizationManager.Get ("TITLE");                  //RaceData.TrackData.Title);
        context.Description = LocalizationManager.Get ("DESCRIPTION");      //RaceData.TrackData.Description);
    }
    #endregion
}

public class LoadBattleSplashScreenContext : Context
{
    #region Properties
    private readonly Property<string> TitleProperty = new Property<string> ();
    public string Title
    {
        get { return TitleProperty.Value; }
        set { TitleProperty.Value = value; }
    }

    private readonly Property<string> DescriptionProperty = new Property<string> ();
    public string Description
    {
        get { return DescriptionProperty.Value; }
        set { DescriptionProperty.Value = value; }
    }
    #endregion
}