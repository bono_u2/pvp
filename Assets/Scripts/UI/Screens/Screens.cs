﻿public class Screens
{
    #region Constants
    public const string MAIN_MENU_SCREEN_NAME = "MainMenuScreen/MainMenuScreen";
    public const string BATTLE_HUD_SCREEN_NAME = "BattleHUDScreen/BattleHUDScreen";
    public const string BATTLE_INPUT_SCREEN_NAME = "BattleInputScreen/BattleInputScreen";
    public const string BATTLE_WIN_SCREEN_NAME = "BattleWinScreen/BattleWinScreen";
    public const string BATTLE_LOSE_SCREEN_NAME = "BattleLoseScreen/BattleLoseScreen";
    #endregion
}