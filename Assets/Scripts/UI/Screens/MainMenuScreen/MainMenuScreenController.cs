﻿using Configurations;
using System.Collections;
using UnityEngine;

public class MainMenuScreenController : ScreenController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnLoadBattleBtnHandler ()
    {
        var mission = GlobalData.Config.Missions[0];
        StartCoroutine (LoadBattle (mission));
    }

    public void OnOpenTestDialogsBtnHandler ()
    {
        StartCoroutine (OpenFirstDialog ());
    }

    public void OnOpenLoggerDialogBtnHandler ()
    {
        DialogsManager.CreateDialog<LoggerDialogController> (Dialogs.LOGGER_DIALOG_NAME);
    }

    public void OnOpenSettingsDialogBtnHandler ()
    {
        DialogsManager.CreateDialog<SettingsDialogController> (Dialogs.SETTINGS_DIALOG_NAME);
    }

    public void OnExitGameBtnHandler ()
    {
        StartCoroutine (OpenExitGameDialog ());
    }
    #endregion

    #region Protected methods
    protected override void KeyEvent (KeyCode _keyCode)
    {
        if (DialogsManager.DialogsCount > 0)
            return;

        if (_keyCode == KeyCode.Escape && IsInited && IsRunning && IsShowed)
            StartCoroutine (OpenExitGameDialog ());
    }
    #endregion

    #region Coroutines
    private IEnumerator LoadBattle (MissionConfig _config)
    {
        // показываем заставку
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadBattleSplashScreenController> (SplashScreens.LOAD_BATTLE_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // прячем текущий скрин
        Hide ();
        yield return WaitHiding ();

        // загружаем сцену с треком
        yield return ScenesManager.LoadSceneAsync (_config.LevelConfig ().Scene); 

        // инициализируем и показываем скрин
        var battleHUDScreen = ScreensManager.CreateScreen<BattleHUDScreenController> (Screens.BATTLE_HUD_SCREEN_NAME);
        yield return battleHUDScreen.WaitShowing ();

        // инициализируем и показываем скрин со стиками
        var battleInputScreen = ScreensManager.CreateScreen<BattleInputScreenController> (Screens.BATTLE_INPUT_SCREEN_NAME);
        yield return battleInputScreen.WaitShowing ();

        // инициализируем BattleManager
        yield return BattleManager.Instance.Init (_config);

        // скрываем заставку
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();

        // закрываем текущий скрин
        Close ();
    }

    private IEnumerator OpenFirstDialog ()
    {
        var data = "Message to TestFirstDialog...";
        var testFirstDialog = DialogsManager.CreateDialog<TestFirstDialogController> (Dialogs.TEST_FIRST_DIALOG_NAME, data);

        yield return testFirstDialog.Wait ();

        var result = testFirstDialog.Result;
        if (result != null)
            Console.Message ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Sender: {testFirstDialog.GetType ().ToString ()}, Result Type: {result.GetType ().ToString ()}, Value = <i>{result.ToString ()}</i></color>", ConsoleChannelType.Debug);
        else
            Console.Warning ($"<color=#dddddd><b>({this.GetType ().ToString ()})</b> Result is null!</color>", ConsoleChannelType.Debug);

        yield return null;
    }

    private IEnumerator OpenExitGameDialog ()
    {
        // показываем диалог и ждем его закрытия
        var exitGameDialog = DialogsManager.CreateDialog<ExitGameDialogController> (Dialogs.EXIT_GAME_DIALOG_NAME);
        yield return exitGameDialog.Wait ();

        // проверяем результат и если он не пустой и true то делаем аборт заезда
        var result = exitGameDialog.Result ?? false;
        if ((bool)result)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit ();
#endif
        }
    }
    #endregion
}