﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;
using System.Collections.Generic;
using System.Collections;

public class BattleWinScreenController : ScreenController
{
    #region Vars
    private BattleWinScreenContext context;
    #endregion

    #region Public methods
    public void OnNextBattleBtnHandler ()
    {
        
    }

    public void OnReturnToMenuBtnHandler ()
    {
        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as BattleWinScreenContext;
    }
    #endregion

    #region Coroutines
    // reserved
    #endregion
}

public class BattleWinScreenContext : Context
{
    #region Properties
    // reserved
    #endregion
}