﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;
using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

public class BattleHUDScreenController : ScreenController
{
    #region Vars
    private BattleHUDScreenContext context;

    private PauseBattleDialogController pauseBattleDialog;

    private PlayerSpawner playerSpawner;

    private Dictionary<BattlePhase, Action<BattlePhaseChangedEvent>> phaseActions = new Dictionary<BattlePhase, Action<BattlePhaseChangedEvent>> ();
    #endregion

    #region Public methods
    public void OnTapToStartBtnHandler ()
    {
        BattleManager.Instance.Phase = BattlePhase.During;
    }

    public void OnPauseBtnHandler ()
    {
        if (!pauseBattleDialog)
            BattleManager.Instance.Phase = BattlePhase.Pause;
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as BattleHUDScreenContext;

        phaseActions.Clear ();
        phaseActions.Add (BattlePhase.Ready, x => BattlePhaseReadyAction (x));
        phaseActions.Add (BattlePhase.Pause, x => BattlePhasePauseAction (x));
        phaseActions.Add (BattlePhase.Win, x => BattlePhaseWinAction (x));
        phaseActions.Add (BattlePhase.Lose, x => BattlePhaseLoseAction (x));
        phaseActions.Add (BattlePhase.Abort, x => BattlePhaseAbortAction (x));


        AppManager.Instance.AddListener<GameEvent> (OnGameEventHandler);

        if (BattleManager.Instance)
            BattleManager.Instance.AddListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);
    }

    protected override void Destroy ()
    {
        if (playerSpawner)
        {
            playerSpawner.Player.Character.RemoveListener<CharacterDieEvent> (OnPlayerDieEventHandler);
            playerSpawner.Player.Character.RemoveListener<CharacterDamageEvent> (OnPlayerDamageHandler);
            playerSpawner.Player.Character.Weapon.RemoveListener<WeaponAmmoAmountChangedEvent> (OnPlayerAmmoAmountChangedHandler);
        }

        if (BattleManager.Instance)
            BattleManager.Instance.RemoveListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);

        AppManager.Instance.RemoveListener<GameEvent> (OnGameEventHandler);
    }

    protected override void KeyEvent (KeyCode _keyCode)
    {
        if (DialogsManager.DialogsCount > 0)
            return;

        if (_keyCode == KeyCode.Escape && IsInited && IsRunning && IsShowed)
            if (!pauseBattleDialog)
                BattleManager.Instance.Phase = BattlePhase.Pause;
    }
    #endregion

    #region Private methods
    private void OnGameEventHandler (FreeTeam.Events.Event _event)
    {
        switch (_event.Type)
        {
            case GameEvent.PAUSE_GAME_EVENT:
                if (!pauseBattleDialog)
                    BattleManager.Instance.Phase = BattlePhase.Pause;
                break;
            default:
                break;
        }
    }

    private void OnBattlePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (BattlePhaseChangedEvent)_event;
        context.BattlePhase = e.NewPhase;

        if (phaseActions.TryGetValue (e.NewPhase, out Action<BattlePhaseChangedEvent> action))
            action.Invoke (e);
    }

    private void OnPlayerDamageHandler (FreeTeam.Events.Event _event)
    {
        var e = (CharacterDamageEvent)_event;
        context.Health = e.Health;
    }

    private void OnPlayerDieEventHandler (FreeTeam.Events.Event _event)
    {
        var e = (CharacterDieEvent)_event;
        var character = (Character)e.Sender;
        character.RemoveListener<CharacterDieEvent> (OnPlayerDieEventHandler);

        BattleManager.Instance.Phase = BattlePhase.Lose;
    }

    private void OnPlayerAmmoAmountChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (WeaponAmmoAmountChangedEvent)_event;
        context.AmmoAmount = e.AmmoAmount;
    }

    //************************************************************************************
    // ACTIONS
    //************************************************************************************

    private void BattlePhaseReadyAction (BattlePhaseChangedEvent _event)
    {
        playerSpawner = FindObjectOfType<PlayerSpawner> ();
        if (playerSpawner)
        {
            playerSpawner.Player.Character.AddListener<CharacterDamageEvent> (OnPlayerDamageHandler);
            playerSpawner.Player.Character.AddListener<CharacterDieEvent> (OnPlayerDieEventHandler);
            playerSpawner.Player.Character.Weapon.AddListener<WeaponAmmoAmountChangedEvent> (OnPlayerAmmoAmountChangedHandler);

            context.Health = playerSpawner.Player.Character.Health;
            context.AmmoAmount = playerSpawner.Player.Character.Weapon.AmmoAmount;
        }
    }

    private void BattlePhasePauseAction (BattlePhaseChangedEvent _event)
    {
        Console.Log ("BattlePhasePauseAction");

        StartCoroutine (OpenPauseBattleDialog ());
    }

    private void BattlePhaseWinAction (BattlePhaseChangedEvent _event)
    {
        Console.Log ("BattlePhaseWinAction");

        StartCoroutine (ShowWinScreen ());
    }

    private void BattlePhaseLoseAction (BattlePhaseChangedEvent _event)
    {
        Console.Log ("BattlePhaseLoseAction");

        StartCoroutine (ShowLoseScreen ());
    }

    private void BattlePhaseAbortAction (BattlePhaseChangedEvent _event)
    {
        Console.Log ("BattlePhaseAbortAction");

        StartCoroutine (ReturnToMenu ());
    }
    #endregion

    #region Coroutines
    private IEnumerator OpenPauseBattleDialog ()
    {
        // ставим физику на паузу
        BattleManager.Instance.PauseBattle ();

        // показываем диалог паузы и ждем его закрытия
        pauseBattleDialog = DialogsManager.CreateDialog<PauseBattleDialogController> (Dialogs.PAUSE_BATTLE_DIALOG_NAME);
        yield return pauseBattleDialog.Wait ();

        // снимаем физику с паузы
        BattleManager.Instance.ResumeBattle ();

        // проверяем результат и если он не пустой и true то делаем аборт
        var result = pauseBattleDialog.Result ?? false;

        // меняем игровую фазу на возврат к игре или на аборт и выхрд в меню
        BattleManager.Instance.Phase = ((bool)result) ? BattlePhase.Abort : BattlePhase.During;

        // чистим ссылку на контроллер диалога
        pauseBattleDialog = null;
    }

    private IEnumerator ShowWinScreen ()
    {
        // показываем RaceRewardScreen
        var battleWinScreen = ScreensManager.CreateScreen<BattleWinScreenController> (Screens.BATTLE_WIN_SCREEN_NAME);
        yield return battleWinScreen.Wait ();

        // проверяем результат и если он не пустой и true то делаем аборт заезда
        var result = battleWinScreen.Result ?? false;
        if ((bool)result)
            yield return NextBattle ();
        else
            yield return ReturnToMenu ();
    }

    private IEnumerator ShowLoseScreen ()
    {
        // показываем RaceRewardScreen
        var battleLoseScreen = ScreensManager.CreateScreen<BattleLoseScreenController> (Screens.BATTLE_LOSE_SCREEN_NAME);
        yield return battleLoseScreen.Wait ();

        // проверяем результат и если он не пустой и true то делаем аборт заезда
        var result = battleLoseScreen.Result ?? false;
        if ((bool)result)
            yield return Restart ();
        else
            yield return ReturnToMenu ();
    }

    private IEnumerator Restart ()
    {
        Console.Info ("Restart");

        var config = BattleManager.Instance.Config;

        // отписываемся от всякого
        Destroy ();

        var battleInputScreen = ScreensManager.FindLastScreen<BattleInputScreenController> ();
        if (battleInputScreen)
        {
            battleInputScreen.Close ();
            yield return battleInputScreen.Wait ();
        }

        // показываем заставку
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadBattleSplashScreenController> (SplashScreens.LOAD_BATTLE_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // выгружаем текущую сцену
        yield return ScenesManager.UnloadAllScenesAsync ();

        // загружаем сцену с треком
        yield return ScenesManager.LoadSceneAsync (config.LevelConfig ().Scene);

        // инитим себя заного
        Init ();

        // инициализируем и показываем скрин со стиками
        battleInputScreen = ScreensManager.CreateScreen<BattleInputScreenController> (Screens.BATTLE_INPUT_SCREEN_NAME);
        yield return battleInputScreen.WaitShowing ();

        // инициализируем BattleManager
        yield return BattleManager.Instance.Init (config);

        // скрываем заставку
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();
    }

    private IEnumerator NextBattle ()
    {
        // TODO : нужно загружать следующий батл
        Console.Info ("NextBattle");

        yield return null;
    }

    private IEnumerator ReturnToMenu ()
    {
        // показываем заставку
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadGameSplashScreenController> (SplashScreens.LOAD_GAME_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // прячем текущий скрин
        Hide ();

        // удаляем скрин с инпутом (стиками)
        var battleInputScreen = ScreensManager.FindLastScreen<BattleInputScreenController> ();
        if (battleInputScreen)
        {
            battleInputScreen.Close ();
            yield return battleInputScreen.Wait ();
        }

        // ждем пока спрячется данный скрин
        yield return WaitHiding ();

        // загружаем сцену
        yield return ScenesManager.LoadSceneAsync (Scenes.MENU_SCENE_NAME);

        // инициализируем и показываем скрин
        var mainMenuScreen = ScreensManager.CreateScreen<MainMenuScreenController> (Screens.MAIN_MENU_SCREEN_NAME);
        yield return mainMenuScreen.WaitShowing ();

        // скрываем заставку
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();

        // закрываем текущий скрин
        Close ();
    }
    #endregion
}

public class BattleHUDScreenContext : Context
{
    #region Properties
    private readonly Property<BattlePhase> BattlePhaseProperty = new Property<BattlePhase> ();
    public BattlePhase BattlePhase
    {
        get { return BattlePhaseProperty.Value; }
        set { BattlePhaseProperty.Value = value; }
    }

    private readonly Property<float> HealthProperty = new Property<float> ();
    public float Health
    {
        get { return HealthProperty.Value; }
        set { HealthProperty.Value = value; }
    }

    private readonly Property<int> AmmoAmountProperty = new Property<int> ();
    public int AmmoAmount
    {
        get { return AmmoAmountProperty.Value; }
        set { AmmoAmountProperty.Value = value; }
    }

    private readonly Property<float> ReloadTimeProperty = new Property<float> ();
    public float ReloadTime
    {
        get { return ReloadTimeProperty.Value; }
        set { ReloadTimeProperty.Value = value; }
    }

    private readonly Property<float> ShotDelayTimeProperty = new Property<float> ();
    public float ShotDelayTime
    {
        get { return ShotDelayTimeProperty.Value; }
        set { ShotDelayTimeProperty.Value = value; }
    }
    #endregion
}