﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    #region Singleton
    private static ScreensManager instance_;
    private static ScreensManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<ScreensManager> ();

            if (instance_ == null)
                Console.Error ($"No <b>{typeof (ScreensManager).FullName}</b> instance set");

            return instance_;
        }
    }
    #endregion

    #region Constants
    private const string SCREEN_PREFABS_PATH = "Ui/Screens/{0}";

    private const string INITIALIZATION_METHOD_NAME = "Initialization";
    private const string DESTROY_METHOD_NAME = "Destroy";
    private const string KEY_EVENT_METHOD_NAME = "KeyEvent";
    #endregion

    #region Vars
    private List<ScreenController> screens = new List<ScreenController> ();
    #endregion

    #region Unity methods
    void OnGUI ()
    {
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyUp)
        {
            var screenController = screens.LastOrDefault ();
            if (screenController)
                screenController.SendMessage (KEY_EVENT_METHOD_NAME, e.keyCode);
        }
    }
    #endregion

    #region Private methods
    private ScreenController Create (string _screenName, object _data = null, bool _keepHistory = true)
    {
        return Create<ScreenController> (_screenName, _data, _keepHistory);
    }

    private T Create<T> (string _screenName, object _data = null, bool _keepHistory = true) where T : ScreenController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_screenName))
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) ScreenName is null or empty!", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (SCREEN_PREFABS_PATH, _screenName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Prefab \"<b>{_screenName}</b>\" no found!", ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var screenGO = Instantiate (prefab, transform, false);

        // Get DialogController component
        var screenController = screenGO.GetComponent<T> ();
        if (!screenController)
        {
            // Debug
            Console.Error ($"({this.GetType ().ToString ()}) Controller \"<b>{typeof (T)}</b>\" in prefab \"<b>{_screenName}</b>\" no found!", ConsoleChannelType.UI);

            // Destroy screen instance
            screenGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        screens.Add (screenController);

        // set dialog Data
        screenController.Data = _data;

        // подписываемся на событие закрытия диалога
        screenController.AddListener (ScreenEventType.Destruction, OnDestructionScreenHandler);

        // Init DialogController
        screenController.SendMessage (INITIALIZATION_METHOD_NAME);

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) Screens count: <b>{screens.Count}</b>", ConsoleChannelType.UI);

        return screenController;
    }

    private T FindFirst<T> () where T : ScreenController
    {
        return screens.OfType<T> ().FirstOrDefault ();
    }

    private T FindLast<T> () where T : ScreenController
    {
        return screens.OfType<T> ().LastOrDefault ();
    }

    private void Destroy ()
    {
        // Last DialogController
        var screenController = screens.Last ();

        Destroy (screenController);
    }

    private void Destroy (ScreenController _screenController)
    {
        // Check DialogController
        if (!_screenController)
        {
            Console.Warning ($"({this.GetType ().ToString ()}) Controller is null!", ConsoleChannelType.UI);
            return;
        }

        // отписываемся от события
        _screenController.RemoveListener (ScreenEventType.Destruction, OnDestructionScreenHandler);

        // Call Closing DialogController
        _screenController.SendMessage (DESTROY_METHOD_NAME);

        // Remove DialogController from dialogs list
        screens.Remove (_screenController);

        // Destroy dialog GameObject
        GameObject.Destroy (_screenController.gameObject);

        // Debug
        Console.Info ($"({this.GetType ().ToString ()}) Screens count: <b>{screens.Count}</b>", ConsoleChannelType.UI);
    }

    private void DestroyAll ()
    {
        for (var i = 0; i < screens.Count; i++)
            Destroy (screens[i]);
    }

    private void OnDestructionScreenHandler (ScreenEvent _event)
    {
        // уничтожаем диалог
        Destroy (_event.Sender);
    }
    #endregion

    #region Public static methods
    public static ScreenController CreateScreen (string _screenName, object _data = null)
    {
        return Instance.Create (_screenName, _data);
    }

    public static T CreateScreen<T> (string _screenName, object _data = null) where T : ScreenController
    {
        return Instance.Create<T> (_screenName, _data);
    }

    public static T FindFirstScreen<T> () where T : ScreenController
    {
        return Instance.FindFirst<T> ();
    }

    public static T FindLastScreen<T> () where T : ScreenController
    {
        return Instance.FindLast<T> ();
    }

    public static void DestroyScreen ()
    {
        Instance.Destroy ();
    }

    public static void DestroyScreen (ScreenController _screenController)
    {
        Instance.Destroy (_screenController);
    }

    public static void DestroyAllScreeens ()
    {
        Instance.DestroyAll ();
    }

    public static int DialogsCount { get { return Instance.screens.Count; } }
    #endregion
}