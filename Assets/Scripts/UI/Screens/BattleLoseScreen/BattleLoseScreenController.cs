﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;

public class BattleLoseScreenController : ScreenController
{
    #region Vars
    private BattleLoseScreenContext context;
    #endregion

    #region Public methods
    public void OnRestartBtnHandler ()
    {
        Result = true;

        Close ();
    }

    public void OnReturnToMenuBtnHandler ()
    {
        Result = false;

        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as BattleLoseScreenContext;
    }
    #endregion

    #region Coroutines
    // reserved
    #endregion
}

public class BattleLoseScreenContext : Context
{
    #region Properties
    // reserved
    #endregion
}