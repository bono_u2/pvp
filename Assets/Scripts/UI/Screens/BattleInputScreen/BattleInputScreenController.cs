﻿using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;
using UnityEngine;

#pragma warning disable 0649
public class BattleInputScreenController : ScreenController
{
    #region Vars
    [SerializeField]
    private Stick leftStick;
    [SerializeField]
    private Stick rightStick;

    private BattleInputScreenContext context;
    #endregion

    #region Get / Set
    public Vector2 LeftStickDirection
    {
        get { return (leftStick) ? leftStick.Direction : (Vector2)Utils.VectorUtils.ZERO; }
    }

    public Vector2 RightStickDirection
    {
        get { return (rightStick) ? rightStick.Direction : (Vector2)Utils.VectorUtils.ZERO; }
    }

    public Stick LeftStick { get { return leftStick; } }
    public Stick RightStick { get { return rightStick; } }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        context = GetComponent<ContextHolder> ().Context as BattleInputScreenContext;

        if (BattleManager.Instance)
            BattleManager.Instance.AddListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);
    }

    protected override void Destroy ()
    {
        if (BattleManager.Instance)
            BattleManager.Instance.RemoveListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);
    }
    #endregion

    #region Private methods
    private void OnBattlePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (BattlePhaseChangedEvent)_event;
        context.BattlePhase = e.NewPhase;

        switch (e.NewPhase)
        {
            case BattlePhase.During:
                Show ();
                break;
            case BattlePhase.Undefined:
            case BattlePhase.Ready:
            case BattlePhase.Win:
            case BattlePhase.Lose:
            case BattlePhase.Pause:
            case BattlePhase.Abort:
            default:
                Hide ();
                break;
        }
    }
    #endregion
}

public class BattleInputScreenContext : Context
{
    #region Properties
    private readonly Property<BattlePhase> BattlePhaseProperty = new Property<BattlePhase> ();
    public BattlePhase BattlePhase
    {
        get { return BattlePhaseProperty.Value; }
        set { BattlePhaseProperty.Value = value; }
    }
    #endregion
}