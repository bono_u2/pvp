﻿using System;

namespace Configurations
{
    [Serializable]
    public class CharacterConfig
    {
        #region Vars
        public string Id;
        public string Name;
        public string Prefab;
        public float Health;
        public string Weapons;
        public float Speed;
        #endregion
    }
}