﻿using System;

namespace Configurations
{
    [Serializable]
    public class MissionConfig
    {
        #region Vars
        public string Id;
        public string Name;
        public string Description;
        public string Level;
        public string[] Enemies;
        public int WavesAmount;
        public float WaveDelay;
        public string EnemiesPerWave;
        public string Params;
        #endregion
    }
}