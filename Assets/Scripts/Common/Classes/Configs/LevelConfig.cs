﻿using System;

namespace Configurations
{
    [Serializable]
    public class LevelConfig
    {
        #region Vars
        public string Id;
        public string Name;
        public string Description;
        public string Scene;
        public string Image;
        #endregion
    }
}