﻿using System;

namespace Configurations
{
    [Serializable]
    public class WeaponConfig
    {
        #region Vars
        public string Id;
        public string Name;
        public string Ammo;
        public float ReloadTime;
        public float RateOfFire;
        public int CartridgeSize;
        public int ExpensePerShot;
        public float Distance;
        #endregion
    }
}