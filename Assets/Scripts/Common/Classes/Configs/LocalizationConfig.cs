﻿using System;

namespace Configurations
{
    [Serializable]
    public class LocalizationConfig
    {
        #region Vars
        public string Key;
        public string RU;
        public string EN;
        #endregion
    }
}