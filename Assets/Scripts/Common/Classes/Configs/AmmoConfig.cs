﻿using System;

namespace Configurations
{
    [Serializable]
    public class AmmoConfig
    {
        #region Vars
        public string Id;
        public string Name;
        public float Damage;
        public float LifeTime;
        public string Prefab;
        public float Speed;
        #endregion
    }
}