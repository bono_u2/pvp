﻿using UnityEngine;

public class Layers
{
    #region Public static vars
    public static int DEFAULT = LayerMask.NameToLayer ("Default");
    public static int TRANSPARENT_FX = LayerMask.NameToLayer ("TransparentFX");
    public static int IGNORE_RAYCAST = LayerMask.NameToLayer ("Ignore Raycast");
    public static int WATER = LayerMask.NameToLayer ("Water");
    public static int UI = LayerMask.NameToLayer ("UI");

    public static int PLAYER = LayerMask.NameToLayer ("Player");
    public static int ENEMIES = LayerMask.NameToLayer ("Enemies");

    public static int BULLETS = LayerMask.NameToLayer ("Bullets");
    public static int OBSTACLES = LayerMask.NameToLayer ("Obstacles");

    public static int TRIGGERS = LayerMask.NameToLayer ("Triggers");

    public static int VFX = LayerMask.NameToLayer ("VFX");
    #endregion
}