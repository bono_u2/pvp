﻿public enum Antialiasing : int
{
    _0 = 0,
    _2 = 2,
    _4 = 4,
    _8 = 8,
}