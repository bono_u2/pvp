﻿using Configurations;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;

public class EnemySpawner : EventMonoBehaviour, ISpawner
{
    #region Vars
    private Coroutine spawnCoroutine;

    private List<CharacterConfig> characterConfigs = new List<CharacterConfig> ();

    private int max = 15;
    #endregion

    #region Unity methods
    void Start ()
    {
        if (BattleManager.Instance)
            BattleManager.Instance.AddListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);
    }

    protected void OnDestroy ()
    {
        if (BattleManager.Instance)
            BattleManager.Instance.RemoveListener<BattlePhaseChangedEvent> (OnBattlePhaseChangedHandler);
    }
    #endregion

    #region Get / Set
    public MissionConfig Config { get; private set; }

    public int QueueCount { get; private set; }
    public int CurrentCount { get; private set; }
    #endregion

    #region Public methods
    public void AddToQueue (int _count)
    {
        QueueCount += _count;
    }

    public void Remove (TopDownPersonCharacter _topDownPersonCharacter)
    {
        ObjectPoolManager.ReleaseObject (_topDownPersonCharacter.gameObject);

        CurrentCount--;
    }
    #endregion

    #region Private methods
    private void OnBattlePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (BattlePhaseChangedEvent)_event;

        switch (e.NewPhase)
        {
            case BattlePhase.During:
                if (spawnCoroutine == null)
                    spawnCoroutine = StartCoroutine (Spawn ());
                break;
            case BattlePhase.Win:
            case BattlePhase.Lose:
            case BattlePhase.Abort:
                StopCoroutine (spawnCoroutine);
                break;
            case BattlePhase.Undefined:
            case BattlePhase.Ready:
            case BattlePhase.Pause:
            default:
                break;
        }
    }
    #endregion

    #region Coroutines
    public IEnumerator Init (MissionConfig _missionConfig)
    {
        Config = _missionConfig;

        for (var i = 0; i < Config.Enemies.Length; i++)
        {
            var characterId = Config.Enemies[i];
            var characterConfig = GlobalData.Config.Characters.FirstOrDefault (x => x.Id == characterId);
            if (characterConfig == null)
                continue;

            characterConfigs.Add (characterConfig);
        }

        yield return null;
    }

    public IEnumerator Spawn ()
    {
        var wait = new WaitForSeconds (0.5f);

        while (true)
        {
            if (QueueCount > 0 && CurrentCount < max)
            {
                var rnd = Utils.MathUtils.RandRange (0, characterConfigs.Count);
                var characterConfig = characterConfigs[rnd];
                var character = new Character (characterConfig);

                var prefab = ResourcesManager.LoadCache<GameObject> (URIs.CHARACTER_PREFABS_URI, characterConfig.Prefab);

                var enemy = ObjectPoolManager.SpawnObject (prefab, transform.position, Quaternion.identity);

                var characterComp = enemy.GetComponent<TopDownPersonCharacter> ();
                characterComp.Init (character, this);

                var characterAIComp = enemy.GetComponent<TopDownPersonAIControl> ();
                characterAIComp.Init ();

                CurrentCount++;

                QueueCount--;
            }

            yield return wait;
        }
    }
    #endregion
}