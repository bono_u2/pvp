﻿using System.Collections;
using System;
using UnityEngine;

public class PlayerSpawner : EventMonoBehaviour, ISpawner
{
    #region Vars
    private TopDownPersonCharacter topDownPersonCharacter;
    #endregion

    #region Public methods
    public void Spawn (ICharacter _character)
    {
        var prefab = ResourcesManager.Load<GameObject> (URIs.CHARACTER_PREFABS_URI, _character.Config.Prefab);
        if (prefab == null)
            throw new Exception ("Player Prefab not found!");

        var characterGO = Instantiate (prefab, this.transform, false);

        topDownPersonCharacter = characterGO.GetComponent<TopDownPersonCharacter> ();
        if (!topDownPersonCharacter)
            throw new Exception ("This is not character!");

        topDownPersonCharacter.Init (_character, this);
    }

    public void Remove (TopDownPersonCharacter _topDownPersonCharacter)
    {
    }
    #endregion

    #region Get / Set
    public TopDownPersonCharacter Player { get { return topDownPersonCharacter; } }
    #endregion

    #region Coroutines
    public IEnumerator Init ()
    {
        var charConfig = GlobalData.Config.Characters.Find (x => x.Id == "player");
        var character = new Character (charConfig);
        Spawn (character);

        yield return null;
    }
    #endregion
}