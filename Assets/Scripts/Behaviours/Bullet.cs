﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    #region Vars
    private Rigidbody rb;

    private float lifeTime;
    private float velocity;
    #endregion

    #region Unity methods
    void Start ()
    {
        rb = GetComponent<Rigidbody> ();
    }

    void FixedUpdate ()
    {
        rb.MovePosition (transform.position + (Direction * velocity * Time.deltaTime));

        if (lifeTime <= 0)
            Destruction ();
        else
            lifeTime -= Time.fixedDeltaTime;
    }

    void OnTriggerEnter (Collider _other)
    {
        Console.Log ($"OnTriggerEnter: {_other.gameObject.name}");

        Destruction ();
    }
    #endregion

    #region Get / Set
    public IAmmo Ammo { get; private set; }
    public Vector3 Direction { get; private set; }
    #endregion

    #region Public methods
    public void Init (IAmmo _ammo, Vector3 _direction)
    {
        Ammo = _ammo;
        Direction = _direction;

        lifeTime = Ammo.Config.LifeTime;
        velocity = Ammo.Config.Speed;
    }
    #endregion

    #region Private methods
    private void Destruction ()
    {
        ObjectPoolManager.ReleaseObject (this.gameObject);
    }
    #endregion
}