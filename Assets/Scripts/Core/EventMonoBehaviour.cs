﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class EventMonoBehaviour : MonoBehaviour
{
    #region Vars
    private Dictionary<Type, FreeTeam.Events.EventWrapper> wrappers = new Dictionary<Type, FreeTeam.Events.EventWrapper> ();
    #endregion

    #region Public methods
    public void AddListener<T> (FreeTeam.Events.EventHandler _listener) where T : FreeTeam.Events.Event
    {
        var type = typeof (T);
        if (!wrappers.TryGetValue (type, out FreeTeam.Events.EventWrapper eventWrapper))
        {
            eventWrapper = new FreeTeam.Events.EventWrapper ();
            eventWrapper.Handler += _listener;
            wrappers.Add (type, eventWrapper);
        }
        else
            eventWrapper.Handler += _listener;
    }

    public void RemoveListener<T> (FreeTeam.Events.EventHandler _listener) where T : FreeTeam.Events.Event
    {
        var type = typeof (T);
        if (wrappers.TryGetValue (type, out FreeTeam.Events.EventWrapper thisEvent))
            thisEvent.Handler -= _listener;
    }

    public void Dispatch (FreeTeam.Events.Event _event)
    {
        var type = _event.GetType ();
        if (wrappers.TryGetValue (type, out FreeTeam.Events.EventWrapper eventWrapper))
            eventWrapper.Invoke (_event);
    }
    #endregion
}