﻿using UnityEngine;

namespace Utils
{
    public class VectorUtils
    {
        public static readonly Vector3 UP = new Vector3 (0.0f, 1.0f, 0.0f);
        public static readonly Vector3 DOWN = new Vector3 (0.0f, -1.0f, 0.0f);
        public static readonly Vector3 FORWARD = new Vector3 (0.0f, 0.0f, 1.0f);
        public static readonly Vector3 BACK = new Vector3 (0.0f, 0.0f, -1.0f);
        public static readonly Vector3 RIGHT = new Vector3 (1.0f, 0.0f, 0.0f);
        public static readonly Vector3 LEFT = new Vector3 (-1.0f, 0.0f, 0.0f);
        public static readonly Vector3 ZERO = new Vector3 (0.0f, 0.0f, 0.0f);
    }
}