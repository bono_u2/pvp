﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utils
{
    public class DrawHandlesUtils
    {
#if UNITY_EDITOR
        public static Vector3 RotatePointAroundPivot (Vector3 _point, Vector3 _pivot, Quaternion _rotation)
        {
            return _rotation * (_point - _pivot) + _pivot;
        }

        public static void DrawRect (Vector3 _position, Quaternion _rotation, Vector2 _size, Vector2 _offset)
        {
            var min = -_size * 0.5f + _offset;
            var max = _size * 0.5f + _offset;

            var xyz = RotatePointAroundPivot (new Vector3 (min.x, min.y, 0f), Vector3.zero, _rotation) + _position;
            var xYz = RotatePointAroundPivot (new Vector3 (min.x, max.y, 0f), Vector3.zero, _rotation) + _position;
            var XYz = RotatePointAroundPivot (new Vector3 (max.x, max.y, 0f), Vector3.zero, _rotation) + _position;
            var Xyz = RotatePointAroundPivot (new Vector3 (max.x, min.y, 0f), Vector3.zero, _rotation) + _position;

            Handles.DrawLine (xyz, xYz);
            Handles.DrawLine (xYz, XYz);
            Handles.DrawLine (XYz, Xyz);
            Handles.DrawLine (Xyz, xyz);
        }

        public static void DrawBox (Vector3 _position, Quaternion _rotation, Vector3 _size)
        {
            var max = _size * 0.5f;
            var min = -max;

            var xyz = RotatePointAroundPivot (new Vector3 (min.x, min.y, min.z), Vector3.zero, _rotation) + _position;
            var xYz = RotatePointAroundPivot (new Vector3 (min.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var XYz = RotatePointAroundPivot (new Vector3 (max.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var Xyz = RotatePointAroundPivot (new Vector3 (max.x, min.y, min.z), Vector3.zero, _rotation) + _position;

            var xyZ = RotatePointAroundPivot (new Vector3 (min.x, min.y, max.z), Vector3.zero, _rotation) + _position;
            var xYZ = RotatePointAroundPivot (new Vector3 (min.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XYZ = RotatePointAroundPivot (new Vector3 (max.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XyZ = RotatePointAroundPivot (new Vector3 (max.x, min.y, max.z), Vector3.zero, _rotation) + _position;

            Handles.DrawLine (xyz, xYz);
            Handles.DrawLine (xYz, XYz);
            Handles.DrawLine (XYz, Xyz);
            Handles.DrawLine (Xyz, xyz);

            Handles.DrawLine (xyZ, xYZ);
            Handles.DrawLine (xYZ, XYZ);
            Handles.DrawLine (XYZ, XyZ);
            Handles.DrawLine (XyZ, xyZ);

            Handles.DrawLine (xyz, xyZ);
            Handles.DrawLine (xYz, xYZ);
            Handles.DrawLine (XYz, XYZ);
            Handles.DrawLine (Xyz, XyZ);
        }
#endif
    }
}