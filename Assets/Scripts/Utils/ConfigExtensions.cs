﻿using Configurations;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System;
using UnityEngine;
using Utils;

public static class ConfigExtensions
{
    public static int EnemyCount (this MissionConfig _config)
    {
        var str = _config.EnemiesPerWave;
        if (int.TryParse (str, out int num))
            return num;

        var pattern = @"(?<min>\d*)-(?<max>\d*)";
        var itemDescription = Regex.Match (str, pattern);
        var validMatch = itemDescription.Groups.Count > 1;
        if (validMatch)
        {
            var min = int.Parse (itemDescription.Groups["min"].Value);
            var max = int.Parse (itemDescription.Groups["max"].Value);

            return MathUtils.RandRange (min, max + 1);
        }

        return 0;
    }

    public static LevelConfig LevelConfig (this MissionConfig _config)
    {
        return GlobalData.Config.Levels.FirstOrDefault (x => x.Id == _config.Level);
    }

    public static AmmoConfig AmmoConfig (this WeaponConfig _config)
    {
        return GlobalData.Config.Ammos.FirstOrDefault (x => x.Id == _config.Ammo);
    }

    public static WeaponConfig WeaponConfig (this CharacterConfig _config)
    {
        return GlobalData.Config.Weapons.FirstOrDefault (x => x.Id == _config.Weapons);
    }
}