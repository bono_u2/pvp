﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class ScriptableObjectUtility
{
#if UNITY_EDITOR
    public static T CreateAsset<T> (bool _fast = false) where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T> ();

        string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if (path == "")
            path = "Assets";
        else if (System.IO.Path.GetExtension (path) != "")
            path = path.Replace (System.IO.Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath ($"{path}/{typeof (T).Name}.asset");

        AssetDatabase.CreateAsset (asset, assetPathAndName);

        if (!_fast)
        {
            AssetDatabase.SaveAssets ();
            AssetDatabase.Refresh ();

            EditorUtility.FocusProjectWindow ();

            Selection.activeObject = asset;
        }

        return asset;
    }

    public static T CreateAsset<T> (string _path) where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T> ();

        AssetDatabase.CreateAsset (asset, _path);
        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh ();

        EditorUtility.FocusProjectWindow ();

        Selection.activeObject = asset;

        return asset;
    }
#endif
}