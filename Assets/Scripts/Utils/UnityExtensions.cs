﻿using UnityEngine;

public static class UnityExtensions
{
    #region Public methods
    public static T SafeDestroy<T> (this T _object) where T : Object
    {
        if (_object != null)
        {

            if (Application.isEditor && !Application.isPlaying)
                Object.DestroyImmediate (_object);
            else
                Object.Destroy (_object);
        }

        return _object;
    }

    public static float GetMinTime (this AnimationCurve _curve)
    {
        return _curve.length > 0 ? _curve.keys[0].time : 0f;
    }

    public static float GetMaxTime (this AnimationCurve _curve)
    {
        return _curve.length > 0 ? _curve.keys[_curve.length - 1].time : 0f;
    }
    #endregion
}