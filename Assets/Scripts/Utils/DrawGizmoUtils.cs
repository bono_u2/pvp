﻿using UnityEngine;

namespace Utils
{
    public class DrawGizmoUtils
    {
        public static Vector3 RotatePointAroundPivot (Vector3 _point, Vector3 _pivot, Quaternion _rotation)
        {
            return _rotation * (_point - _pivot) + _pivot;
        }

        public static Vector3 RotatePointAroundPivot (Vector3 _point, Vector3 _pivot, Vector3 _angles)
        {
            return Quaternion.Euler (_angles) * (_point - _pivot) + _pivot;
        }

        public static void DrawRect (Vector3 _position, Vector2 _size, Vector2 _offset, Quaternion _rotation)
        {
            var min = -_size * 0.5f + _offset;
            var max = _size * 0.5f + _offset;

            var xyz = RotatePointAroundPivot (new Vector3 (min.x, min.y, 0f), Vector3.zero, _rotation) + _position;
            var xYz = RotatePointAroundPivot (new Vector3 (min.x, max.y, 0f), Vector3.zero, _rotation) + _position;
            var XYz = RotatePointAroundPivot (new Vector3 (max.x, max.y, 0f), Vector3.zero, _rotation) + _position;
            var Xyz = RotatePointAroundPivot (new Vector3 (max.x, min.y, 0f), Vector3.zero, _rotation) + _position;

            Gizmos.DrawLine (xyz, xYz);
            Gizmos.DrawLine (xYz, XYz);
            Gizmos.DrawLine (XYz, Xyz);
            Gizmos.DrawLine (Xyz, xyz);
        }

        public static void DrawBox (Bounds _rect, Vector3 _position, Vector3 _rotation, Color _color)
        {
            var min = _rect.min;
            var max = _rect.max;

            var xyz = RotatePointAroundPivot (new Vector3 (min.x, min.y, min.z), Vector3.zero, _rotation) + _position;
            var xYz = RotatePointAroundPivot (new Vector3 (min.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var XYz = RotatePointAroundPivot (new Vector3 (max.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var Xyz = RotatePointAroundPivot (new Vector3 (max.x, min.y, min.z), Vector3.zero, _rotation) + _position;

            var xyZ = RotatePointAroundPivot (new Vector3 (min.x, min.y, max.z), Vector3.zero, _rotation) + _position;
            var xYZ = RotatePointAroundPivot (new Vector3 (min.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XYZ = RotatePointAroundPivot (new Vector3 (max.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XyZ = RotatePointAroundPivot (new Vector3 (max.x, min.y, max.z), Vector3.zero, _rotation) + _position;

            Gizmos.color = _color;
            Gizmos.DrawLine (xyz, xYz);
            Gizmos.DrawLine (xYz, XYz);
            Gizmos.DrawLine (XYz, Xyz);
            Gizmos.DrawLine (Xyz, xyz);

            Gizmos.DrawLine (xyZ, xYZ);
            Gizmos.DrawLine (xYZ, XYZ);
            Gizmos.DrawLine (XYZ, XyZ);
            Gizmos.DrawLine (XyZ, xyZ);

            Gizmos.DrawLine (xyz, xyZ);
            Gizmos.DrawLine (xYz, xYZ);
            Gizmos.DrawLine (XYz, XYZ);
            Gizmos.DrawLine (Xyz, XyZ);
        }

        public static void DrawBox (Bounds _rect, Vector3 _position, Quaternion _rotation, Color _color)
        {
            var min = _rect.min;
            var max = _rect.max;

            var xyz = RotatePointAroundPivot (new Vector3 (min.x, min.y, min.z), Vector3.zero, _rotation) + _position;
            var xYz = RotatePointAroundPivot (new Vector3 (min.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var XYz = RotatePointAroundPivot (new Vector3 (max.x, max.y, min.z), Vector3.zero, _rotation) + _position;
            var Xyz = RotatePointAroundPivot (new Vector3 (max.x, min.y, min.z), Vector3.zero, _rotation) + _position;

            var xyZ = RotatePointAroundPivot (new Vector3 (min.x, min.y, max.z), Vector3.zero, _rotation) + _position;
            var xYZ = RotatePointAroundPivot (new Vector3 (min.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XYZ = RotatePointAroundPivot (new Vector3 (max.x, max.y, max.z), Vector3.zero, _rotation) + _position;
            var XyZ = RotatePointAroundPivot (new Vector3 (max.x, min.y, max.z), Vector3.zero, _rotation) + _position;

            Gizmos.color = _color;
            Gizmos.DrawLine (xyz, xYz);
            Gizmos.DrawLine (xYz, XYz);
            Gizmos.DrawLine (XYz, Xyz);
            Gizmos.DrawLine (Xyz, xyz);

            Gizmos.DrawLine (xyZ, xYZ);
            Gizmos.DrawLine (xYZ, XYZ);
            Gizmos.DrawLine (XYZ, XyZ);
            Gizmos.DrawLine (XyZ, xyZ);

            Gizmos.DrawLine (xyz, xyZ);
            Gizmos.DrawLine (xYz, xYZ);
            Gizmos.DrawLine (XYz, XYZ);
            Gizmos.DrawLine (Xyz, XyZ);
        }
    }
}