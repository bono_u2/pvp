﻿using UnityEngine.SceneManagement;

public class SceneEvent
{
    public SceneEvent (Scene _scene, SceneEventType _type)
    {
        Scene = _scene;
        Type = _type;
    }

    #region Get / Set
    public Scene Scene { get; private set; }
    public SceneEventType Type { get; private set; }
    #endregion

}