﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ScenesManager : MonoBehaviour
{
    #region Singleton
    private static ScenesManager instance_;
    public static ScenesManager Instance
    {
        get
        {
            if (!instance_)
                instance_ = FindObjectOfType<ScenesManager> ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private List<Scene> scenes = new List<Scene> () { };

    private Dictionary<string, SceneEventWrapper> wrappers = new Dictionary<string, SceneEventWrapper> ();
    #endregion

    #region Private methods
    private bool Contains (string _toLoad)
    {
        return scenes.Any (x => x.name == _toLoad);
    }

    private void AddListener (Scene _scene, SceneEventType _eventType, SceneEventWrapper.SceneEventHandler _listener)
    {
        var key = $"{_scene.name}{_eventType.ToString ()}";

        if (!wrappers.TryGetValue (key, out SceneEventWrapper eventWrapper))
        {
            eventWrapper = new SceneEventWrapper ();
            eventWrapper.Handler += _listener;
            wrappers.Add (key, eventWrapper);
        }
        else
            eventWrapper.Handler += _listener;
    }

    private void RemoveListener (Scene _scene, SceneEventType _eventType, SceneEventWrapper.SceneEventHandler _listener)
    {
        var key = $"{_scene.name}{_eventType.ToString ()}";

        if (wrappers.TryGetValue (key, out SceneEventWrapper thisEvent))
            thisEvent.Handler -= _listener;
    }

    private void Dispatch (SceneEvent _event)
    {
        var key = $"{_event.Scene.name}{_event.Type.ToString ()}";

        if (wrappers.TryGetValue (key, out SceneEventWrapper eventWrapper))
            eventWrapper.Invoke (_event);
    }
    #endregion

    #region Coroutines
    private IEnumerator LoadAsync (string _sceneName, LoadSceneMode _loadSceneMode = LoadSceneMode.Additive)
    {
        if (string.IsNullOrEmpty (_sceneName))
            yield break;

        if (ContainsScene (_sceneName))
            yield break;

        yield return UnloadAllAsync ();

        Console.Log ($"Loading scene: <b>{_sceneName}</b>");

        var loading = SceneManager.LoadSceneAsync (_sceneName, _loadSceneMode);
        while (!loading.isDone)
        {
            yield return null;
        }

        var scene = SceneManager.GetSceneByName (_sceneName);
        if (scene.IsValid ())
        {
            SceneManager.SetActiveScene (scene);

            Dispatch (new SceneEvent (scene, SceneEventType.Init));

            Instance.scenes.Add (scene);
        }
    }

    private IEnumerator UnloadAllAsync ()
    {
        // удаляем все кроеме главной сцены асинхронно
        var scenes = Instance.scenes;
        var length = scenes.Count;
        for (int i = 0; i < length; i++)
        {
            var scene = scenes[i];
            scenes.Remove (scene);

            Console.Log ($"Unload scene: <b>{scene.name}</b>");

            Dispatch (new SceneEvent (scene, SceneEventType.Destruction));

            SceneManager.UnloadSceneAsync (scene);
        }

        // чтобы не ждать удаления каждой из сцен последовательно, ждем здесь удаления их всех сразу
        yield return new WaitWhile (() => SceneManager.sceneCount > 1);
    }
    #endregion

    #region Public static methods
    public static bool ContainsScene (string _sceneName)
    {
        return Instance.Contains (_sceneName);
    }

    public static void AddEventListener (Scene _scene, SceneEventType _eventType, SceneEventWrapper.SceneEventHandler _listener)
    {
        Instance.AddListener (_scene, _eventType, _listener);
    }

    public static void RemoveEventListener (Scene _scene, SceneEventType _eventType, SceneEventWrapper.SceneEventHandler _listener)
    {
        Instance.RemoveListener (_scene, _eventType, _listener);
    }

    public static void DispatchEvent (SceneEvent _event)
    {
        Instance.Dispatch (_event);
    }
    #endregion

    #region Public static coroutines
    public static IEnumerator LoadSceneAsync (string _sceneName, LoadSceneMode _loadSceneMode = LoadSceneMode.Additive)
    {
        yield return Instance.LoadAsync (_sceneName, _loadSceneMode);
    }

    public static IEnumerator UnloadAllScenesAsync ()
    {
        yield return Instance.UnloadAllAsync ();
    }
    #endregion
}

public class SceneEventWrapper
{
    #region Delegates
    public delegate void SceneEventHandler (SceneEvent _event);
    #endregion

    #region Vars
    public event SceneEventHandler Handler;
    #endregion

    #region Public methods
    public void Invoke (SceneEvent _event)
    {
        Handler?.Invoke (_event);
    }
    #endregion
}