﻿using Configurations;
using System.Collections.Generic;
using System;

[Serializable]
public class Config
{
    #region Vars
    public List<LocalizationConfig> Localizations = new List<LocalizationConfig> ();
    public List<WeaponConfig> Weapons = new List<WeaponConfig> ();
    public List<AmmoConfig> Ammos = new List<AmmoConfig> ();
    public List<CharacterConfig> Characters = new List<CharacterConfig> ();
    public List<LevelConfig> Levels = new List<LevelConfig> ();
    public List<MissionConfig> Missions = new List<MissionConfig> ();
    #endregion

    #region Public static methods
    public static Config Load (string _configString)
    {
        var config = UnityEngine.JsonUtility.FromJson<Config> (_configString);

        return config;
    }
    #endregion
}