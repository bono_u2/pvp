﻿using System.Collections;
using UnityEngine;

public class AppManager : EventMonoBehaviour
{
    #region Singleton
    private static AppManager instance_;
    public static AppManager Instance
    {
        get
        {
            if (!instance_)
                instance_ = FindObjectOfType<AppManager> ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private bool isInitialized;
    #endregion

    #region Unity methods
    void Start ()
    {
        // подписываемся на логирование движком
        Application.logMessageReceivedThreaded += CaptureLogThread;

        // инициализируем локализацию
        LocalizationManager.Instance.Init ();

        // Загружаем нужные части игры
        InitGame ();
    }

    void OnDestroy ()
    {
        // отписка от логирования движком
        Application.logMessageReceivedThreaded -= CaptureLogThread;
    }

    void OnApplicationPause (bool _pauseStatus)
    {
        Console.Log ($"({this.GetType ().ToString ()}) PauseStatus: <b>{_pauseStatus}</b>");

        if (!isInitialized)
            return;

        if (_pauseStatus)
            Dispatch (new GameEvent (this, GameEvent.PAUSE_GAME_EVENT));
        else
            Dispatch (new GameEvent (this, GameEvent.RESUME_GAME_EVENT));
    }

    void OnApplicationFocus (bool _focus)
    {
        Console.Log ($"({this.GetType ().ToString ()}) Focus: <b>{_focus}</b>");
    }

    void OnApplicationQuit ()
    {
        Console.Info ($"({this.GetType ().ToString ()}) OnApplicationQuit");

        if (!isInitialized)
            return;

        // сохраняем прогресс если нужно
    }
    #endregion

    #region Private methods
    private void InitGame ()
    {
        StartCoroutine (InitGameRoutine ());
    }

    private void CaptureLogThread (string _condition, string _stacktrace, LogType _type)
    {
        if (_type == LogType.Warning)
        {
            var message = _condition + "\n" + _stacktrace;
            Console.Warning (message);
        }
        else if (_type == LogType.Log || _type == LogType.Assert)
        {
            // reserved
        }
        else if (_type == LogType.Error)
        {
            var message = "Not critical ERROR:" + _condition + "\n" + _stacktrace;
            Console.Error (message);
        }
        else
        {
            var message = "Unhandled EXCEPTION:" + _condition + "\n" + _stacktrace;
            Console.Error (message);

            SplashScreensManager.DestroyAllSplashScreeens ();

            var dialogController = DialogsManager.CreateDialog<ErrorDialogController> (Dialogs.ERROR_DIALOG_NAME, "ERROR_UNKNOWN", _condition + "\r\n" + _stacktrace);
            StartCoroutine (dialogController.Wait ().Then (InitGame));
        }
    }
    #endregion

    #region Coroutines
    private IEnumerator InitGameRoutine ()
    {
        // закрываем все сплэши
        SplashScreensManager.DestroyAllSplashScreeens ();
        // закрываем все скрины
        ScreensManager.DestroyAllScreeens ();
        // закрываем все диалоги
        DialogsManager.DestroyAllDialogs ();

        // выгружаем все ненужные сцены
        yield return ScenesManager.UnloadAllScenesAsync ();

        // показываем заставку
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadGameSplashScreenController> (SplashScreens.LOAD_GAME_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // отправляем событие начала инициализации приложения
        Dispatch (new GameEvent (this, GameEvent.PREINITIALIZE_GAME_EVENT));

        // загружаем конфиг
        yield return GlobalData.LoadConfigAsync (true);

        // загружаем локализацию
        LocalizationManager.Instance.Load ();

        // Загружаем и применяем игровые настройки
        SettingsManager.Instance.Load ();
        SettingsManager.Instance.Apply ();
        SettingsManager.Instance.ApplyResolution ();

        // открываем главное меню
        var mainMenuScreen = ScreensManager.CreateScreen<MainMenuScreenController> (Screens.MAIN_MENU_SCREEN_NAME);
        yield return mainMenuScreen.WaitShowing ();

        // загружаем сцену меню
        yield return ScenesManager.LoadSceneAsync (Scenes.MENU_SCENE_NAME);

        // проставляем флаг что мы успешно инициализировались
        isInitialized = true;

        // отправляем событие окончания инициализации приложения
        Dispatch (new GameEvent (this, GameEvent.INITIALIZE_GAME_EVENT));

        // скрываем заставку
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();
    }
    #endregion
}