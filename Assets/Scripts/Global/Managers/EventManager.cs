﻿using FreeTeam.Events;

public class EventManager : EventDispatcher
{
    #region Instance
    private static EventManager _instance;
    public static EventManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = new EventManager ();

            return _instance;
        }
    }
    #endregion

    #region Public static methods
    public static void AddEventListener<T> (EventHandler _listener) where T : Event
    {
        Instance.AddListener<T> (_listener);
    }

    public static void RemoveEventListener<T> (EventHandler _listener) where T : Event
    {
        Instance.RemoveListener<T> (_listener);
    }

    public static void DispatchEvent (Event _event)
    {
        Instance.Dispatch (_event);
    }
    #endregion
}