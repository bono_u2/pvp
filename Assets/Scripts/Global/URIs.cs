﻿using System.IO;

public class URIs
{
    #region Constants
    public const string CONFIG_URI = @"Configs/Config";
    public const string LOCALIZATION_STANDBY_URI = @"Configs/LocalizationStandby";
    public const string CAMERA_ANIMATIONS_URI = @"Configs/CameraAnimations/{0}";

    public const string FONT_URI = @"Fonts/{0}";
    public const string FONT_TEXTURES_URI = @"Textures/Fonts/{0}";

    public const string COMMON_TEXTURES_URI = @"Textures/Common/{0}";

    public const string CUBEMAPS_URI = @"Cubemaps/{0}";

    public const string CHARACTER_PREFABS_URI = @"Prefabs/Characters/{0}";
    public const string AMMO_PREFABS_URI = @"Prefabs/Ammo/{0}";
    public const string VFX_PREFAB_URI = @"Prefabs/VFX/{0}";
    #endregion

    #region Static vars
    public static string CACHE_PATH = UnityEngine.Application.persistentDataPath + "/";
    public static string CACHE_VISUAL_PATH = CACHE_PATH + "CacheV/";

    public static string SETTINGS_PATH = Path.Combine (UnityEngine.Application.persistentDataPath, @"settings");
    #endregion
}