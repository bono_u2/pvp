﻿using System;

public class GameErrorEvent : FreeTeam.Events.Event
{
    #region Static vars
    public const string ID = "GameError";
    #endregion

    public GameErrorEvent (object _sender, string _message, string _techInfo = null) : base (_sender, ID)
    {
        Message = _message;
        TechInfo = _techInfo ?? Environment.StackTrace;
    }

    #region Get / Set
    public string Message { get; private set; }
    public string TechInfo { get; private set; }
    #endregion
}