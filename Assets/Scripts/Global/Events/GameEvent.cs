using FreeTeam.Events;

public class GameEvent : Event
{
    #region Constants
    public const string PREINITIALIZE_GAME_EVENT = "PreInitialize";
    public const string INITIALIZE_GAME_EVENT = "Initialize";
    public const string RESTART_GAME_EVENT = "Restart";
    public const string PAUSE_GAME_EVENT = "Pause";
    public const string RESUME_GAME_EVENT = "Resume";
    #endregion

    public GameEvent (object _sender, string _id, params object[] _params) : base (_sender, _id)
    {
        Params = _params;
    }

    #region Get / Set
    public object[] Params { get; private set; }
    #endregion
}