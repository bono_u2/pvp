﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;

#pragma warning disable 0649
public static class GlobalData
{
    #region Static vars
    public static bool IsDebug;

    public static Config Config = null;
    #endregion

    #region Static methods
    // reserved
    #endregion

    #region Static coroutines
    public static IEnumerator LoadConfigAsync (bool _forced = false)
    {
        if (Config == null || _forced)
        {
            Console.Info ("<color=white>Load config async from <b>RESOURCES FOLDER</b></color>...", ConsoleChannelType.Resources);

            var content = ResourcesManager.Load<TextAsset> (URIs.CONFIG_URI).text;

            yield return new DoInBackground (() =>
            {
                try
                {
                    Config = Config.Load (content);
                }
                catch (Exception e)
                {
                    Console.Error (e.Message);
                    AppManager.Instance.Dispatch (new GameErrorEvent (null, "Error read config file!"));
                    Config = null;
                }
            });
        }
    }
    #endregion
}