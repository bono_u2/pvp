﻿using System.Collections;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    #region Vars
    public float distance = 10.0f;
    public Vector3 offsetPosition = new Vector3 (0.0f, 0.0f, 0.0f);
    public Vector3 defaultRotation = new Vector3 (80.0f, 0.0f, 0.0f);
    public float rotationDampening = 3.0f;

    private float xDeg = 0.0f;
    private float yDeg = 0.0f;

    private float currentDistance;

    private Transform target;
    private Vector3 targetOffset;

    private Vector3 position;
    private Quaternion rotation;
    #endregion

    #region Unity methods
    void Start ()
    {
    }

    void FixedUpdate ()
    {
        if (!target)
            return;

        Calculate ();
    }

    void Update ()
    {
        transform.SetPositionAndRotation (position, rotation);
    }
    #endregion

    #region Private methods
    private void Calculate ()
    {
        // lerp angles
        float currentRotationAngleX = Mathf.LerpAngle (transform.eulerAngles.y, xDeg, Time.fixedDeltaTime * rotationDampening);
        float currentRotationAngleY = Mathf.LerpAngle (transform.eulerAngles.x, yDeg, Time.fixedDeltaTime * rotationDampening);

        rotation = Quaternion.Euler (currentRotationAngleY, currentRotationAngleX, 0f);
        position = (target.position + targetOffset) - (rotation * Vector3.forward * currentDistance);
    }

    private void SetPosition (Vector3 _offset, Vector3 _rotation)
    {
        targetOffset = _offset;
        transform.rotation = Quaternion.Euler (_rotation);

        xDeg = transform.eulerAngles.y;
        yDeg = transform.eulerAngles.x;
    }
    #endregion

    #region Coroutines
    public IEnumerator Init ()
    {
        var watch = new System.Diagnostics.Stopwatch ();
        watch.Start ();


        // set target
        target = FindObjectOfType<TopDownPersonControl> ().transform;

        // reset pos
        SetPosition (offsetPosition, defaultRotation);

        // set distance
        currentDistance = distance;


        watch.Stop ();
        Console.Info ($"<b>({this.GetType ().ToString ()})</b> InitTime: <b><i>{watch.Elapsed.TotalSeconds:N6}s.</i></b>", ConsoleChannelType.Stopwatch);

        yield return null;
    }
    #endregion
}