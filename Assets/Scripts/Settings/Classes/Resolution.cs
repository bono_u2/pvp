﻿using System;

[Serializable]
public class Resolution
{
    #region Get / Set
    public int width;
    public int height;
    #endregion

    public Resolution () { }
    public Resolution (int _width, int _height)
    {
        width = _width;
        height = _height;
    }

    #region Public methods
    public bool Equals (Resolution _resolution)
    {
        if (ReferenceEquals (_resolution, null))
            return false;

        if (ReferenceEquals (this, _resolution))
            return true;

        return (width == _resolution.width && height == _resolution.height);
    }

    public override bool Equals (object obj)
    {
        if (ReferenceEquals (null, obj))
            return false;

        return obj is Resolution && Equals ((Resolution)obj);
    }

    public override int GetHashCode ()
    {
        return 0;
    }

    public override string ToString ()
    {
        return string.Format ("{0}x{1}", width, height);
    }
    #endregion

    #region Operators
    public static bool operator == (Resolution _resolution0, Resolution _resolution1)
    {
        if (ReferenceEquals (_resolution0, _resolution1))
            return true;

        if (ReferenceEquals (_resolution0, null))
            return false;

        if (ReferenceEquals (_resolution1, null))
            return false;

        return (_resolution0.width == _resolution1.width && _resolution0.height == _resolution1.height);
    }

    public static bool operator != (Resolution _resolution0, Resolution _resolution1)
    {
        return !(_resolution0 == _resolution1);
    }
    #endregion
}