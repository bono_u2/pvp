﻿using System;
using UnityEngine;

[Serializable]
public class GameSettings
{
    #region Vars
    public SystemLanguage Language = Application.systemLanguage;
    #endregion
}