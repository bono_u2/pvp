﻿using System;
using UnityEngine;

[Serializable]
public class GraphicsSettings
{
    #region Constants
    public const int TARGET_FRAMERATE_MIN = 30;
    public const int TARGET_FRAMERATE_MAX = 60;
    public const int TARGET_FRAMERATE_UNLIM = 999;  // 999 - unrestricted fps
    #endregion

    #region Vars
    public Resolution Resolution;
    public GraphicsDetailsType GraphicsDetails = GraphicsDetailsType.Best;
    public Antialiasing Antialiasing = Antialiasing._2;

    public int TargetFrameRate = TARGET_FRAMERATE_MIN;

    public bool PostProcess = false;
    #endregion
}