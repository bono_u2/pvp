﻿using System;
using UnityEngine;

[Serializable]
public class OtherSettings
{
    #region Vars
    public bool Vibration = false;
    #endregion
}