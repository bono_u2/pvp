﻿using System;
using UnityEngine;

[Serializable]
public class Settings
{
    #region Vars
    public AudioSettings AudioSettings = new AudioSettings ();
    public GraphicsSettings GraphicsSettings = new GraphicsSettings ();
    public GameSettings GameSettings = new GameSettings ();
    public OtherSettings OtherSettings = new OtherSettings ();
    #endregion
}