﻿using System;
using UnityEngine;

[Serializable]
public class AudioSettings
{
    #region Vars
    public bool SoundEnabled = true;
    public bool MusicEnabled = true;
    #endregion
}