﻿using FreeTeam.Events;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class SettingsManager : EventDispatcher
{
    #region Instance
    private static SettingsManager instance_;
    public static SettingsManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new SettingsManager ();

            return instance_;
        }
    }
    #endregion

    #region Constants
    const string SIMPLE_GRAPHICS_KEYWORD_STR = "SIMPLE_GRAPHICS";
    const string BEST_GRAPHICS_KEYWORD_STR = "BEST_GRAPHICS";
    #endregion

    #region Vars
    public Settings settings;
    #endregion

    public SettingsManager ()
    {
        // сохраняем дефолтное разрешение экрана
        DefaultResolution = new Resolution (Screen.width, Screen.height);
        // сохраняем текущее разрешение экрана
        CurrentResolution = DefaultResolution;
    }

    #region Get / Set
    public Resolution DefaultResolution { get; private set; }
    public Resolution CurrentResolution { get; private set; }
    #endregion

    #region Public methods
    public void Apply ()
    {
        LocalizationManager.Change (settings.GameSettings.Language);
        QualitySettings.antiAliasing = (int)settings.GraphicsSettings.Antialiasing;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = settings.GraphicsSettings.TargetFrameRate;

        Dispatch (new SettingsChangedEvent (this));
    }

    public void ApplyResolution ()
    {
        var newResolution = settings.GraphicsSettings.Resolution;
        if (CurrentResolution == newResolution)
            return;

        Console.Log ($"Changed resolution: <b>\"{newResolution.ToString ()}\"</b>");

        CurrentResolution = newResolution;
        Screen.SetResolution (CurrentResolution.width, CurrentResolution.height, true);
    }

    public void Load ()
    {
        try
        {
            var jsonString = LoadFile (URIs.SETTINGS_PATH);
            if (!string.IsNullOrEmpty (jsonString))
                settings = JsonUtility.FromJson<Settings> (jsonString);
        }
        catch (Exception e)
        {
            Console.Warning (e.Message);
            AppManager.Instance.Dispatch (new GameErrorEvent (this, "Error read file!"));
        }

        if (settings == null)
        {
            settings = new Settings ();
            settings.GraphicsSettings.Resolution = GetOptimalScreenResolution ();

            Save ();
        }
    }

    public void Save ()
    {
        try
        {
            var jsonString = JsonUtility.ToJson (settings, true);
            if (!string.IsNullOrEmpty (jsonString))
                SaveFile (URIs.SETTINGS_PATH, jsonString);
        }
        catch (Exception e)
        {
            Console.Warning (e.Message);
            AppManager.Instance.Dispatch (new GameErrorEvent (this, "Error write file!"));
        }
    }
    #endregion

    #region Private methods
    private void SetShaderDetails (GraphicsDetailsType _graphicsDetails)
    {
        Console.Log ($"Changed graphics details: <b>\"{_graphicsDetails}\"</b>");

        switch (_graphicsDetails)
        {
            case GraphicsDetailsType.Simple:
                Shader.EnableKeyword (SIMPLE_GRAPHICS_KEYWORD_STR);
                Shader.DisableKeyword (BEST_GRAPHICS_KEYWORD_STR);
                break;
            case GraphicsDetailsType.Best:
                Shader.DisableKeyword (SIMPLE_GRAPHICS_KEYWORD_STR);
                Shader.EnableKeyword (BEST_GRAPHICS_KEYWORD_STR);
                break;
        }
    }
    #endregion

    #region Public static methods
    public static List<Resolution> GetAvailableResolutions ()
    {
        var DefaultResolution = Instance.DefaultResolution;
        return new List<Resolution> () {
            DefaultResolution,
            new Resolution ((int)(DefaultResolution.width * 0.75f), (int)(DefaultResolution.height * 0.75f)),
            new Resolution ((int)(DefaultResolution.width * 0.5f), (int)(DefaultResolution.height * 0.5f)),
        };
    }

    public static Resolution GetOptimalScreenResolution ()
    {
        var DefaultResolution = Instance.DefaultResolution;

#if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR
        return DefaultResolution;
#elif UNITY_ANDROID
        return GetAvailableResolutions ()[1];
#endif
    }

    public static void SaveFile (string _fullPath, string _json)
    {
        using (FileStream sourceStream = new FileStream (_fullPath, FileMode.Create, FileAccess.Write))
        using (StreamWriter writer = new StreamWriter (sourceStream))
        {
            writer.Write (_json);
        }
    }

    public static string LoadFile (string _fullPath)
    {
        if (!File.Exists (_fullPath))
            return "";

        var result = "";
        using (FileStream sourceStream = new FileStream (_fullPath, FileMode.Open, FileAccess.Read))
        using (StreamReader targetStream = new StreamReader (sourceStream))
        {
            result = targetStream.ReadToEnd ();
        }
        return result;
    }
    #endregion
}