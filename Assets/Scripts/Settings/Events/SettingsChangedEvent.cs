﻿using FreeTeam.Events;

public class SettingsChangedEvent : Event
{
    #region Static vars
    public const string ID = "SettingsChanged";
    #endregion

    public SettingsChangedEvent (object _sender) : base (_sender, ID) { }
}