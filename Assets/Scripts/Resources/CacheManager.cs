﻿using System.Collections.Generic;
using UnityEngine;

public class CacheManager
{
    #region Instance
    private static CacheManager instance_;
    private static CacheManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new CacheManager ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private Dictionary<string, Object> cache = new Dictionary<string, Object> ();
    #endregion

    public CacheManager () : base () { }

    #region Private methods
    private bool ContainsKeyInCache (string _key)
    {
        return cache.ContainsKey (_key);
    }

    private void AddObjectToCache (string _key, Object _object)
    {
        if (cache.ContainsKey (_key))
            cache[_key] = _object;
        else
            cache.Add (_key, _object);
    }

    private void RemoveObjectFromCache (string _key)
    {
        cache.Remove (_key);
    }

    private T GetObjectFromCache<T> (string _key, Object _default = null) where T : Object
    {
        if (cache.TryGetValue (_key, out Object o))
            return (T)o;

        return (T)_default;
    }

    private bool GetObjectFromCache<T> (string _key, out T _object) where T : Object
    {
        if (cache.TryGetValue (_key, out Object o))
        {
            _object = (T)o;
            return true;
        }

        _object = null;

        return false;
    }

    private bool GetObjectFromCache<T> (string _key, out T _object, Object _default = null) where T : Object
    {
        if (cache.TryGetValue (_key, out Object o))
        {
            _object = (T)o;
            return true;
        }

        _object = (T)_default;

        return false;
    }

    private void ClearCache ()
    {
        cache.Clear ();
    }
    #endregion

    #region Public static methods
    public static bool ContainsKey (string _key)
    {
        return Instance.ContainsKeyInCache (_key);
    }

    public static void Add (string _key, Object _object)
    {
        Instance.AddObjectToCache (_key, _object);
    }

    public static void Remove (string _key)
    {
        Instance.RemoveObjectFromCache (_key);
    }

    public static T Get<T> (string _key) where T : Object
    {
        return Instance.GetObjectFromCache<T> (_key);
    }

    public static bool Get<T> (string _key, out T _object) where T : Object
    {
        return Instance.GetObjectFromCache<T> (_key, out _object);
    }

    public static bool Get<T> (string _key, out T _object, Object _default = null) where T : Object
    {
        return Instance.GetObjectFromCache<T> (_key, out _object, _default);
    }

    public static void Clear ()
    {
        Instance.ClearCache ();
    }
    #endregion
}