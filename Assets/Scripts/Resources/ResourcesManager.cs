﻿using UnityEngine;

public static class ResourcesManager
{
    #region Public static methods
    /// <summary>
    /// Загружает ресурс
    /// </summary>
    public static T Load<T> (string _basePath, params object[] _pathParameters) where T : Object
    {
        if (string.IsNullOrEmpty (_basePath))
            return null;

        var path = ToPath (_basePath, _pathParameters);

        var result = Resources.Load<T> (path);
        if (result == null)
            Console.Warning ($"Loading from resources failed, path: {path}");

        return result;
    }

    /// <summary>
    /// Загружает ресурс и кэширует его в памяти
    /// </summary>
    public static T LoadCache<T> (string _basePath, params object[] _pathParameters) where T : Object
    {
        if (string.IsNullOrEmpty (_basePath))
            return null;

        T result;

        var path = ToPath (_basePath, _pathParameters);
        var keyHash = path;

        var isMeshCacheAvailable = CacheManager.ContainsKey (keyHash);
        if (!isMeshCacheAvailable)
        {
            result = Resources.Load<T> (path);
            if (result == null)
                Console.Warning ($"Loading from resources failed, path: {path}");

            CacheManager.Add (keyHash, result);
        }
        else
            result = CacheManager.Get<T> (keyHash);

        return result;
    }
    #endregion

    #region Private static methods
    private static string ToPath (string basePath, params object[] parameters)
    {
        return parameters.Length > 0 ? string.Format (basePath, parameters) : basePath;
    }
    #endregion
}