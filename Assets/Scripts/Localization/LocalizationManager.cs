﻿using Configurations;
using FreeTeam.Events;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class LocalizationManager : EventDispatcher
{
    #region Classes
    [Serializable]
    class LocalizationsStandby
    {
        public List<LocalizationConfig> Localizations = new List<LocalizationConfig> ();
    }
    #endregion

    #region Singleton
    private static LocalizationManager instance_;
    public static LocalizationManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new LocalizationManager ();

            return instance_;
        }
    }
    #endregion

    #region Static vars
    public static SystemLanguage[] SupportedLanguages = new[]
    {
        SystemLanguage.English,
        SystemLanguage.Russian,
    };
    #endregion

    #region Vars
    // кэшированная локализация
    private Dictionary<string, LocalizationConfig> localizations = new Dictionary<string, LocalizationConfig> ();

    // текущий язык
    private SystemLanguage currentLanguage = SystemLanguage.English;
    #endregion

    public LocalizationManager ()
    {
    }

    #region Get / Set
    public static SystemLanguage CurrentLanguage { get { return Instance.currentLanguage; } }
    #endregion

    #region Public methids
    public void Init ()
    {
        // назначаем по умолчанию разделителем в дробных числах . а не , (нужно для сериализации)
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone ();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        // пробуем загрузить системно-резервную локализацию
        var localizationJson = ResourcesManager.Load<TextAsset> (URIs.LOCALIZATION_STANDBY_URI);
        if (localizationJson != null)
        {
            var localizationsStandby = JsonUtility.FromJson<LocalizationsStandby> (localizationJson.text);
            var locDic = localizationsStandby.Localizations.Where (x => !string.IsNullOrEmpty (x.Key)).ToDictionary (x => x.Key, y => y);
            localizations = localizations.Concat (locDic.Where (x => !localizations.Keys.Contains (x.Key))).ToDictionary (x => x.Key, x => x.Value);
        }

        Console.Info ("LocalizationManager is inited...");
    }

    public void Load ()
    {
        // конвертим List в Dict и обновляем то что уже есть с локализацией из конфига
        var configLoc = GlobalData.Config.Localizations.Where (x => !string.IsNullOrEmpty (x.Key)).ToDictionary (x => x.Key, y => y);
        localizations = localizations.Concat (configLoc).GroupBy (x => x.Key).ToDictionary (x => x.Key, x => x.Last ().Value);

        Console.Info ("LocalizationManager is loaded...");
    }
    #endregion

    #region Private methods
    private void ChangeLanguage (SystemLanguage _language)
    {
        var validatedLanguage = ValidateLanguage (_language);
        if (validatedLanguage != currentLanguage)
        {
            currentLanguage = validatedLanguage;

            Dispatch (new LanguageChangedEvent (this, currentLanguage));
        }
    }

    private string GetLocalization (string _key, params object[] _parameters)
    {
        // удаляем начальные и конечные пробелы, 
        var key = _key.Trim ().ToLowerInvariant ();

        // проверяем на пустоту ключа
        if (string.IsNullOrEmpty (key))
            return _key;

        // пробуем искать перевод
        if (localizations.TryGetValue (key, out LocalizationConfig translation))
        {
            var result = Get (translation);

            // подставляем параметры, если строка была с параметрами
            result = result.Replace ("\\n", Environment.NewLine);
            result = _parameters.Length > 0 ? string.Format (result, _parameters) : result;

            return result;
        }

        return _key;
    }

    private string Get (LocalizationConfig _config)
    {
        string result;
        switch (CurrentLanguage)
        {
            default:
            case SystemLanguage.English:
                result = _config.EN;
                break;
            case SystemLanguage.Russian:
                result = _config.RU;
                break;
        }

        // если нет локализации, возвращаем ключ
        if (string.IsNullOrEmpty (result))
            result = _config.Key;

        return result;
    }
    #endregion

    #region Public static methods
    public static void Change (SystemLanguage _language)
    {
        Instance.ChangeLanguage (_language);
    }

    public static string Get (string _key, params object[] _parameters)
    {
        return Instance.GetLocalization (_key, _parameters);
    }
    #endregion

    #region Private static methods
    private static SystemLanguage ValidateLanguage (SystemLanguage _language)
    {
        if (SupportedLanguages.Contains (_language))
            return _language;

        return SupportedLanguages[0];
    }
    #endregion
}