﻿using UnityEngine;

public class LanguageChangedEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "LanguageChanged";
    #endregion

    public LanguageChangedEvent (object _sender, SystemLanguage _language) : base (_sender, ID)
    {
        Language = _language;
    }

    #region Get / Set
    public SystemLanguage Language { get; private set; }
    #endregion
}