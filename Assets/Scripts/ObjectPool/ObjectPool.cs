﻿using System.Collections.Generic;
using System;

public class ObjectPool<T>
{
    #region Vars
    private List<ObjectPoolContainer<T>> list;
    private Dictionary<T, ObjectPoolContainer<T>> lookup;
    private Func<T> factoryFunc;
    private int lastIndex = 0;
    #endregion

    public ObjectPool (Func<T> _factoryFunc, int _initialSize)
    {
        factoryFunc = _factoryFunc;

        list = new List<ObjectPoolContainer<T>> (_initialSize);
        lookup = new Dictionary<T, ObjectPoolContainer<T>> (_initialSize);

        Warm (_initialSize);
    }

    #region Get / Set
    public int Count
    {
        get { return list.Count; }
    }

    public int CountUsedItems
    {
        get { return lookup.Count; }
    }
    #endregion

    #region Public methods
    public T GetItem ()
    {
        ObjectPoolContainer<T> container = null;
        for (int i = 0; i < list.Count; i++)
        {
            lastIndex++;
            if (lastIndex > list.Count - 1)
                lastIndex = 0;

            if (!list[lastIndex].Used)
            {
                container = list[lastIndex];
                break;
            }
            else
                continue;
        }

        if (container == null)
            container = CreateConatiner ();

        container.Consume ();

        lookup.Add (container.Item, container);

        return container.Item;
    }

    public void ReleaseItem (object item)
    {
        ReleaseItem ((T)item);
    }

    public void ReleaseItem (T item)
    {
        if (lookup.ContainsKey (item))
        {
            var container = lookup[item];
            container.Release ();
            lookup.Remove (item);
        }
        else
            Console.Warning ($"This object pool does not contain the item provided: {item}");
    }
    #endregion

    #region Private methods
    private void Warm (int _capacity)
    {
        for (int i = 0; i < _capacity; i++)
            CreateConatiner ();
    }

    private ObjectPoolContainer<T> CreateConatiner ()
    {
        var container = new ObjectPoolContainer<T> () { Item = factoryFunc () };
        list.Add (container);
        return container;
    }
    #endregion
}