using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    #region Singleton
    private static ObjectPoolManager instance_;
    public static ObjectPoolManager Instance
    {
        get
        {
            if (!instance_)
                instance_ = FindObjectOfType<ObjectPoolManager> ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    public Transform root;

    private Dictionary<GameObject, ObjectPool<GameObject>> prefabLookup;
    private Dictionary<GameObject, ObjectPool<GameObject>> instanceLookup;
    #endregion

    #region Untiy methods
    void Awake ()
    {
        prefabLookup = new Dictionary<GameObject, ObjectPool<GameObject>> ();
        instanceLookup = new Dictionary<GameObject, ObjectPool<GameObject>> ();
    }
    #endregion

    #region Public methods
    public void Warm (GameObject _prefab, int _size)
    {
        if (prefabLookup.ContainsKey (_prefab))
            return;

        var pool = new ObjectPool<GameObject> (() => { return InstantiatePrefab (_prefab); }, _size);
        prefabLookup[_prefab] = pool;
    }

    public GameObject Spawn (GameObject _prefab)
    {
        return Spawn (_prefab, Vector3.zero, Quaternion.identity);
    }

    public GameObject Spawn (GameObject _prefab, Vector3 _position, Quaternion _rotation)
    {
        if (!prefabLookup.ContainsKey (_prefab))
            WarmPool (_prefab, 1);

        var pool = prefabLookup[_prefab];

        var clone = pool.GetItem ();
        clone.transform.position = _position;
        clone.transform.rotation = _rotation;
        clone.SetActive (true);

        instanceLookup.Add (clone, pool);

        return clone;
    }

    public void Release (GameObject _clone)
    {
        _clone.SetActive (false);

        if (instanceLookup.ContainsKey (_clone))
        {
            instanceLookup[_clone].ReleaseItem (_clone);
            instanceLookup.Remove (_clone);
        }
    }

    public void PrintStatus ()
    {
        var strs = new List<string> ();
        foreach (KeyValuePair<GameObject, ObjectPool<GameObject>> keyVal in prefabLookup)
            strs.Add ($"Object Pool for Prefab: {keyVal.Key.name} In Use: {keyVal.Value.CountUsedItems} Total: {keyVal.Value.Count}");

        Console.Log (string.Join ("\r\n", strs));
    }
    #endregion

    #region Private methods
    private GameObject InstantiatePrefab (GameObject _prefab)
    {
        var go = Instantiate (_prefab);
        if (root != null)
            go.transform.parent = root;

        return go;
    }
    #endregion

    #region Public static methods
    public static void WarmPool (GameObject _prefab, int _size)
    {
        Instance.Warm (_prefab, _size);
    }

    public static GameObject SpawnObject (GameObject _prefab)
    {
        return Instance.Spawn (_prefab);
    }

    public static GameObject SpawnObject (GameObject _prefab, Vector3 _position, Quaternion _rotation)
    {
        return Instance.Spawn (_prefab, _position, _rotation);
    }

    public static void ReleaseObject (GameObject _clone)
    {
        Instance.Release (_clone);
    }
    #endregion
}