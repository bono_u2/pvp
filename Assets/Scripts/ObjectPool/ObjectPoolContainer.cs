﻿public class ObjectPoolContainer<T>
{
    #region Get / Set
    public bool Used { get; private set; }

    public T Item { get; set; }
    #endregion

    #region public methods
    public void Consume ()
    {
        Used = true;
    }

    public void Release ()
    {
        Used = false;
    }
    #endregion
}