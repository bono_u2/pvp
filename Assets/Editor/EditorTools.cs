﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class EditorTools
{
    [MenuItem ("Tools/Open path SaveData", false, 0)]
    public static void OpenPathSaveData ()
    {
        Console.Info (Application.persistentDataPath);

        var path = Application.persistentDataPath.Replace (@"/", @"\");
        System.Diagnostics.Process.Start ("explorer.exe", "/select," + path);
    }
}