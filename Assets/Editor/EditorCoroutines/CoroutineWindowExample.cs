﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace EditorCoroutines
{
    public class CoroutineWindowExample : EditorWindow
    {
        //[MenuItem ("Window/Coroutine Example")]
        //public static void ShowWindow ()
        //{
        //    EditorWindow.GetWindow (typeof (CoroutineWindowExample));
        //}

        void OnGUI ()
        {
            if (GUILayout.Button ("Start"))
                this.StartCoroutine (Example ());

            if (GUILayout.Button ("Start WWW"))
                this.StartCoroutine (ExampleWebRequest ());

            if (GUILayout.Button ("Start Nested"))
                this.StartCoroutine (ExampleNested ());

            if (GUILayout.Button ("Stop"))
                this.StopCoroutine ("Example");

            if (GUILayout.Button ("Stop all"))
                this.StopAllCoroutines ();

            if (GUILayout.Button ("Also"))
                this.StopAllCoroutines ();
        }

        IEnumerator Example ()
        {
            while (true)
            {
                Debug.Log ("Hello EditorCoroutine!");
                yield return new WaitForSeconds (2f);
            }
        }

        IEnumerator ExampleWebRequest ()
        {
            while (true)
            {
                var webRequest = new UnityWebRequest ("https://unity3d.com/");
                yield return webRequest.SendWebRequest ();

                if (webRequest.isNetworkError || webRequest.isHttpError)
                    Debug.Log (webRequest.error);
                else
                    Debug.Log ("Hello EditorCoroutine!" + webRequest.downloadHandler.text);

                yield return new WaitForSeconds (2f);
            }
        }

        IEnumerator ExampleNested ()
        {
            while (true)
            {
                yield return new WaitForSeconds (2f);
                Debug.Log ("I'm not nested");
                yield return this.StartCoroutine (ExampleNestedOneLayer ());
            }
        }

        IEnumerator ExampleNestedOneLayer ()
        {
            yield return new WaitForSeconds (2f);
            Debug.Log ("I'm one layer nested");
            yield return this.StartCoroutine (ExampleNestedTwoLayers ());
        }

        IEnumerator ExampleNestedTwoLayers ()
        {
            yield return new WaitForSeconds (2f);
            Debug.Log ("I'm two layers nested");
        }


        class NonEditorClass
        {
            public void DoSomething (bool start, bool stop, bool stopAll)
            {
                if (start)
                    EditorCoroutines.StartCoroutine (Example (), this);

                if (stop)
                    EditorCoroutines.StopCoroutine ("Example", this);

                if (stopAll)
                    EditorCoroutines.StopAllCoroutines (this);
            }

            IEnumerator Example ()
            {
                while (true)
                {
                    Debug.Log ("Hello EditorCoroutine!");
                    yield return new WaitForSeconds (2f);
                }
            }
        }
    }
}