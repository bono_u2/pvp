﻿using UnityEditor;
using UnityEngine;

public class EditorGUIElements
{
    #region Static vars
    private static Texture2D greyTexture;
    #endregion

    #region Public static methods
    public static void SeparateLine (RectOffset _margin = null)
    {
        if (greyTexture == null)
            greyTexture = Utils.TextureUtils.CreateTexture (2, 2, Utils.Colors.dimgray, false, false);

        var splitter = new GUIStyle ();
        splitter.normal.background = greyTexture;
        splitter.stretchWidth = true;
        splitter.margin = (_margin != null) ? _margin : new RectOffset (5, 5, 5, 5);

        GUILayout.Label ("", splitter, GUILayout.Height (1f));
    }

    public static void Separator (bool _withLine = false)
    {
        EditorGUILayout.Space ();

        if (_withLine)
        {
            SeparateLine ();
            EditorGUILayout.Space ();
        }
    }

    public static Rect DrawTextureWithBorder (float _xoffset, float _yoffset, float _width, float _height, string _label, Texture2D _texture, bool _blended)
    {
        Rect border = GUILayoutUtility.GetRect (_width, _height);
        border.width = _width;
        border.x += _xoffset;
        border.y += _yoffset;
        GUI.Box (border, _label, "HelpBox");
        border.x += 1;
        border.y += 1;
        border.width -= 2;
        border.height -= 2;
        if (_texture != null)
            GUI.DrawTexture (border, _texture, ScaleMode.StretchToFill, _blended);

        return border;
    }
    #endregion
}